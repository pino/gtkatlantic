/*
 *     gtkatlantic - the gtk+ monopd client, enjoy network monopoly games
 *
 *
 *  Copyright © 2002-2015 Sylvain Rochet
 *
 *  gtkatlantic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; see the file COPYING. If not, see
 *  <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <gtk/gtk.h>

#include <libxml/parser.h>

#include "load.h"
#include "global.h"
#include "parser.h"

/* parse a config file */
gint8 parse_file_config(gchar *filename)  {

	gchar str[512], *ident, *tmp;
	FILE *file;
	char *ret;

	file = fopen(filename, "r");
	if (!file) return -1;

	for (ret = fgets(str, 512, file); ret && !feof(file); ret = fgets(str, 512, file)) {

		ident = parser_get_identifier(str);
		if(!ident) continue;

		if(! strcmp(ident, "player") )  {

			tmp = parser_get_data(str, "nickname");
			if (tmp) {
				if(config->nickname) g_free(config->nickname);
				config->nickname = tmp;
			}

		} else if(! strcmp(ident, "metaserver") )  {

			tmp = parser_get_data(str, "autoconnect");
			if (tmp) {
				config->metaserver_autoconnect = atoi( (gchar*)tmp );
				g_free(tmp);
			}

			tmp = parser_get_data(str, "sendclientversion");
			if (tmp) {
				config->metaserver_sendclientversion = atoi( (gchar*)tmp );
				g_free(tmp);
			}

		} else if(! strcmp(ident, "customserver") )  {

			tmp = parser_get_data(str, "host");
			if (tmp) {
				if(config->getgames_host) g_free(config->getgames_host);
				config->getgames_host = tmp;
			}

			tmp = parser_get_data(str, "port");
			if (tmp) {
				config->getgames_port = atoi( (gchar*)tmp );
				g_free(tmp);
			}

			tmp = parser_get_data(str, "autoconnect");
			if (tmp) {
				config->getgames_autoconnect = atoi( (gchar*)tmp );
				g_free(tmp);
			}

		} else if(! strcmp(ident, "interface") )  {

			tmp = parser_get_data(str, "playerlist");
			if (tmp) {
				if(! strcmp( tmp, "left") )
					config->game_playerlist_position = GAME_PLAYERLIST_POS_LEFT;
				if(! strcmp( tmp, "right") )
					config->game_playerlist_position = GAME_PLAYERLIST_POS_RIGHT;
				g_free(tmp);
			}

			tmp = parser_get_data(str, "token_speed");
			if (tmp) {
				config->game_token_animation_speed = atoi( (gchar*)tmp );
				g_free(tmp);
			}

			tmp = parser_get_data(str, "token_transparency");
			if (tmp) {
				config->game_token_transparency = atoi( (gchar*)tmp );
				g_free(tmp);
			}
		}

		g_free(ident);
	}

	fclose(file);
	return 0;
}


/* parse the data file interface.xml */
gint8 xmlparse_file_interface()  {

	xmlDocPtr doc;
	xmlNodePtr cur, cur2;
	guint8 ident;
	guint32 color;
	gint32 xoffset, yoffset;
	xmlChar *tmp;

	doc = xmlParseFile(PACKAGE_DATA_DIR "/interface.xml");
	if (doc == NULL) return -1;

	cur = xmlDocGetRootElement(doc);
	if (cur == NULL) {
		xmlFreeDoc(doc);
		return -1;
	}

	if( xmlStrcmp(cur->name, DATAFILE_XMLROOTELEMENT) )  {
		xmlFreeDoc(doc);
		return -1;
	}

	cur = cur->xmlChildrenNode;
	do {

		/* ===== PNGFILES ===== */
		/* ==================== */

		/* parse pngfile_board */
		if(! xmlStrcmp(cur->name, (xmlChar*)"pngfile_board") )  {

			data->pngfile_board_filename = (gchar*)xmlGetProp(cur, (xmlChar*)"filename");

			cur2 = cur->xmlChildrenNode;
			do {

				if(! xmlStrcmp(cur2->name, (xmlChar*)"buffer") )  {

					tmp = xmlGetProp(cur2, (xmlChar*)"id");
					ident = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"x");
					data->pngfile_board_x[ident]  = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"y");
					data->pngfile_board_y[ident]  = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"width");
					data->pngfile_board_width[ident]  = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"height");
					data->pngfile_board_height[ident] = atoi( (gchar*)tmp );
					g_free(tmp);
				}

				cur2 = cur2 -> next;
			} while(cur2 != NULL);
		}

		/* parse pngfile_tokens */
		if(! xmlStrcmp(cur->name, (xmlChar*)"pngfile_tokens") )  {

			data->pngfile_token_filename = (gchar*)xmlGetProp(cur, (xmlChar*)"filename");

			cur2 = cur->xmlChildrenNode;
			do {

				if(! xmlStrcmp(cur2->name, (xmlChar*)"buffer") )  {

					tmp = xmlGetProp(cur2, (xmlChar*)"id");
					ident = atoi( (gchar*)tmp );
					g_free(tmp);
					if (ident > data->token_max) {
						data->token_max = ident;
					}

					tmp = xmlGetProp(cur2, (xmlChar*)"x");
					data->pngfile_token_x[ident]  = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"y");
					data->pngfile_token_y[ident]  = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"width");
					data->pngfile_token_width[ident]  = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"height");
					data->pngfile_token_height[ident] = atoi( (gchar*)tmp );
					g_free(tmp);
				}

				cur2 = cur2 -> next;
			} while(cur2 != NULL);
		}

		/* parse pngfile_cards */
		if(! xmlStrcmp(cur->name, (xmlChar*)"pngfile_cards") )  {

			data->pngfile_card_filename = (gchar*)xmlGetProp(cur, (xmlChar*)"filename");

			cur2 = cur->xmlChildrenNode;
			do {

				if(! xmlStrcmp(cur2->name, (xmlChar*)"buffer") )  {

					tmp = xmlGetProp(cur2, (xmlChar*)"id");
					ident = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"x");
					data->pngfile_card_x[ident]  = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"y");
					data->pngfile_card_y[ident]  = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"width");
					data->pngfile_card_width[ident]  = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"height");
					data->pngfile_card_height[ident] = atoi( (gchar*)tmp );
					g_free(tmp);
				}

				cur2 = cur2 -> next;
			} while(cur2 != NULL);
		}

		/* parse pngfile_stars */
		if(! xmlStrcmp(cur->name, (xmlChar*)"pngfile_stars") )  {

			data->pngfile_star_filename = (gchar*)xmlGetProp(cur, (xmlChar*)"filename");

			cur2 = cur->xmlChildrenNode;
			do {

				if(! xmlStrcmp(cur2->name, (xmlChar*)"buffer") )  {

					tmp = xmlGetProp(cur2, (xmlChar*)"id");
					ident = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"x");
					data->pngfile_star_x[ident]  = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"y");
					data->pngfile_star_y[ident]  = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"width");
					data->pngfile_star_width[ident]  = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"height");
					data->pngfile_star_height[ident] = atoi( (gchar*)tmp );
					g_free(tmp);
				}

				cur2 = cur2 -> next;
			} while(cur2 != NULL);
		}

		/* parse pngfile_stars_m (mortgaged) */
		if(! xmlStrcmp(cur->name, (xmlChar*)"pngfile_stars_m") )  {

			data->pngfile_star_m_filename = (gchar*)xmlGetProp(cur, (xmlChar*)"filename");

			cur2 = cur->xmlChildrenNode;
			do {

				if(! xmlStrcmp(cur2->name, (xmlChar*)"buffer") )  {

					tmp = xmlGetProp(cur2, (xmlChar*)"id");
					ident = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"x");
					data->pngfile_star_m_x[ident]  = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"y");
					data->pngfile_star_m_y[ident]  = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"width");
					data->pngfile_star_m_width[ident]  = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"height");
					data->pngfile_star_m_height[ident] = atoi( (gchar*)tmp );
					g_free(tmp);
				}

				cur2 = cur2 -> next;
			} while(cur2 != NULL);
		}

		/* parse pngfile_commands */
		if(! xmlStrcmp(cur->name, (xmlChar*)"pngfile_commands") )  {

			data->pngfile_command_filename = (gchar*)xmlGetProp(cur, (xmlChar*)"filename");

			cur2 = cur->xmlChildrenNode;
			do {

				if(! xmlStrcmp(cur2->name, (xmlChar*)"buffer") )  {

					tmp = xmlGetProp(cur2, (xmlChar*)"id");
					ident = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"x");
					data->pngfile_command_x[ident]  = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"y");
					data->pngfile_command_y[ident]  = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"width");
					data->pngfile_command_width[ident]  = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"height");
					data->pngfile_command_height[ident] = atoi( (gchar*)tmp );
					g_free(tmp);
				}

				cur2 = cur2 -> next;
			} while(cur2 != NULL);
		}

		/* parse pngfile_horiz_houses */
		if(! xmlStrcmp(cur->name, (xmlChar*)"pngfile_horiz_houses") )  {

			data->pngfile_horiz_house_filename = (gchar*)xmlGetProp(cur, (xmlChar*)"filename");

			cur2 = cur->xmlChildrenNode;
			do {

				if(! xmlStrcmp(cur2->name, (xmlChar*)"buffer") )  {

					tmp = xmlGetProp(cur2, (xmlChar*)"id");
					ident = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"x");
					data->pngfile_horiz_house_x[ident]  = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"y");
					data->pngfile_horiz_house_y[ident]  = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2,(xmlChar*) "width");
					data->pngfile_horiz_house_width[ident]  = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"height");
					data->pngfile_horiz_house_height[ident] = atoi( (gchar*)tmp );
					g_free(tmp);
				}

				cur2 = cur2 -> next;
			} while(cur2 != NULL);
		}

		/* parse pngfile_vert_houses */
		if(! xmlStrcmp(cur->name, (xmlChar*)"pngfile_vert_houses") )  {

			data->pngfile_vert_house_filename = (gchar*)xmlGetProp(cur, (xmlChar*)"filename");

			cur2 = cur->xmlChildrenNode;
			do {

				if(! xmlStrcmp(cur2->name, (xmlChar*)"buffer") )  {

					tmp = xmlGetProp(cur2, (xmlChar*)"id");
					ident = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"x");
					data->pngfile_vert_house_x[ident]  = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"y");
					data->pngfile_vert_house_y[ident]  = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"width");
					data->pngfile_vert_house_width[ident]  = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"height");
					data->pngfile_vert_house_height[ident] = atoi( (gchar*)tmp );
					g_free(tmp);
				}

				cur2 = cur2 -> next;
			} while(cur2 != NULL);
		}


		/* ===== INTERFACE ===== */
		/* ===================== */

		/* parse board interface data */
		if(! xmlStrcmp(cur->name, (xmlChar*)"display_board") )  {

			data->number_estates = 0;

			tmp = xmlGetProp(cur, (xmlChar*)"width");
			data->board_width = atoi( (gchar*)tmp );
			g_free(tmp);

			tmp = xmlGetProp(cur, (xmlChar*)"height");
			data->board_height = atoi( (gchar*)tmp );
			g_free(tmp);

			tmp = xmlGetProp(cur, (xmlChar*)"center_x");
			data->board_center_x = atoi( (gchar*)tmp );
			g_free(tmp);

			tmp = xmlGetProp(cur, (xmlChar*)"center_y");
			data->board_center_y = atoi( (gchar*)tmp );
			g_free(tmp);

			tmp = xmlGetProp(cur, (xmlChar*)"center_width");
			data->board_center_width = atoi( (gchar*)tmp );
			g_free(tmp);

			tmp = xmlGetProp(cur, (xmlChar*)"center_height");
			data->board_center_height = atoi( (gchar*)tmp );
			g_free(tmp);

			cur2 = cur->xmlChildrenNode;

			do {

				if(! xmlStrcmp(cur2->name, (xmlChar*)"estate") )  {

					tmp = xmlGetProp(cur2, (xmlChar*)"id");
					ident = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"x");
					data->estate[ident].x = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"y");
					data->estate[ident].y = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"buffer_board");
					data->estate[ident].buffer_board = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"x1token");
					data->estate[ident].x1token = data->estate[ident].x + atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"y1token");
					data->estate[ident].y1token = data->estate[ident].y + atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"x2token");
					data->estate[ident].x2token = data->estate[ident].x + atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"y2token");
					data->estate[ident].y2token = data->estate[ident].y + atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"x1jail");
					data->estate[ident].x1jail = data->estate[ident].x + atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"y1jail");
					data->estate[ident].y1jail = data->estate[ident].y + atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"x2jail");
					data->estate[ident].x2jail = data->estate[ident].x + atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"y2jail");
					data->estate[ident].y2jail = data->estate[ident].y + atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"xstar");
					data->estate[ident].xstar = data->estate[ident].x + atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"ystar");
					data->estate[ident].ystar = data->estate[ident].y + atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"xhouse");
					data->estate[ident].xhouse = data->estate[ident].x + atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"yhouse");
					data->estate[ident].yhouse = data->estate[ident].y + atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"type_house");
					if(! xmlStrcmp(tmp, (xmlChar*)"horizontal") )
						data->estate[ident].type_house = TYPE_HOUSE_HORIZONTAL;
					else if(! xmlStrcmp(tmp, (xmlChar*)"vertical") )
						data->estate[ident].type_house = TYPE_HOUSE_VERTICAL;
					else
						data->estate[ident].type_house = TYPE_HOUSE_NONE;
					g_free(tmp);

					data->number_estates++;
				}

				cur2 = cur2 -> next;
			} while(cur2 != NULL);
		}

		/* parse player token interface data */
		if(! xmlStrcmp(cur->name, (xmlChar*)"display_playertoken") )  {

			tmp = xmlGetProp(cur, (xmlChar*)"width");
			data->playerlist_token_width = atoi( (gchar*)tmp );
			g_free(tmp);

			tmp = xmlGetProp(cur, (xmlChar*)"height");
			data->playerlist_token_height = atoi( (gchar*)tmp );
			g_free(tmp);
		}

		/* parse player cards interface data */
		if(! xmlStrcmp(cur->name, (xmlChar*)"display_playercard") )  {

			data->number_playerlist_card = 0;

			tmp = xmlGetProp(cur, (xmlChar*)"width");
			data->playerlist_cards_width = atoi( (gchar*)tmp );
			g_free(tmp);

			tmp = xmlGetProp(cur, (xmlChar*)"height");
			data->playerlist_cards_height = atoi( (gchar*)tmp );
			g_free(tmp);

			tmp = xmlGetProp(cur, (xmlChar*)"xoffset");
			xoffset = atoi( (gchar*)tmp );
			g_free(tmp);

			tmp = xmlGetProp(cur, (xmlChar*)"yoffset");
			yoffset = atoi( (gchar*)tmp );
			g_free(tmp);

			tmp = xmlGetProp(cur, (xmlChar*)"alphaunowned");
			if(! g_ascii_strncasecmp((gchar*)tmp, "0x", 2) )  {

				sscanf((gchar*)tmp, "0x%2X", &color);
				data->playerlist_cards_alphaunowned = color;
			}
			else data->playerlist_cards_alphaunowned = atoi( (gchar*)tmp );
			g_free(tmp);

			tmp = xmlGetProp(cur, (xmlChar*)"alphaowned");
			if(! g_ascii_strncasecmp((gchar*)tmp, "0x", 2) )  {

				sscanf((gchar*)tmp, "0x%2X", &color);
				data->playerlist_cards_alphaowned = color;
			}
			else data->playerlist_cards_alphaowned = atoi( (gchar*)tmp );
			g_free(tmp);

			tmp = xmlGetProp(cur, (xmlChar*)"alphamortgage");
			if(! g_ascii_strncasecmp((gchar*)tmp, "0x", 2) )  {

				sscanf((gchar*)tmp, "0x%2X", &color);
				data->playerlist_cards_alphamortgage = color;
			}
			else data->playerlist_cards_alphamortgage = atoi( (gchar*)tmp );
			g_free(tmp);

			tmp = xmlGetProp(cur, (xmlChar*)"cardbgcolor");
			sscanf((gchar*)tmp, "0x%6X", &color);
			data->playerlist_cards_cardbgcolor = color;
			g_free(tmp);

			tmp = xmlGetProp(cur, (xmlChar*)"cardbgcolormortgage");
			sscanf((gchar*)tmp, "0x%6X", &color);
			data->playerlist_cards_cardbgcolormortgage = color;
			g_free(tmp);

			cur2 = cur->xmlChildrenNode;

			do {

				if(! xmlStrcmp(cur2->name, (xmlChar*)"card") )  {

					tmp = xmlGetProp(cur2, (xmlChar*)"id");
					ident = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"x");
					data->playerlist_card[ident].x = atoi( (gchar*)tmp ) + xoffset;
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"y");
					data->playerlist_card[ident].y = atoi( (gchar*)tmp ) + yoffset;
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"z");
					data->playerlist_card[ident].z = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"buffer_card");
					data->playerlist_card[ident].buffer_card = atoi( (gchar*)tmp );
					g_free(tmp);

					tmp = xmlGetProp(cur2, (xmlChar*)"estateid");
					data->playerlist_card[ident].estateid = atoi( (gchar*)tmp );
					g_free(tmp);

					data->number_playerlist_card++;
				}

				cur2 = cur2 -> next;
			} while(cur2 != NULL);
		}

		cur = cur -> next;

	} while(cur != NULL);

	xmlFreeDoc(doc);
	return 0;
}


/* create subregion of a pixbuf; copy to a new pixbuf, so we are sure
 * the destination pixbuf is RGBA32 */
static GdkPixbuf *pixbuf_crop(GdkPixbuf *pixbuf, int x, int y, int width, int height)
{
	GdkPixbuf *sub;

	sub = gdk_pixbuf_new(GDK_COLORSPACE_RGB, TRUE, 8, width, height);
	if (!sub) return NULL;
	gdk_pixbuf_copy_area(pixbuf, x, y, width, height, sub, 0, 0);

	return sub;
}


/* read all game pngs images */
gint8 game_load_pngs()   {
	GdkPixbuf *pixbuf = NULL;
	GError *err = NULL;
	gchar *filename;
	guint32 i;
	gint8 ret = 0;

	if (data->png_game_loaded) {
		return 0;
	}

	/* ---- load board file */
	filename = g_strconcat(PACKAGE_DATA_DIR "/", data->pngfile_board_filename, NULL);
	pixbuf = gdk_pixbuf_new_from_file(filename, &err);
	if (!pixbuf) {
		ret = -1;
		goto free_and_return;
	}

	for (i = 0 ; data->pngfile_board_width[i] ; i++) {
		data->pngfile_board_buf[i] = pixbuf_crop(pixbuf, data->pngfile_board_x[i], data->pngfile_board_y[i], data->pngfile_board_width[i], data->pngfile_board_height[i]);
	}

	g_free(filename);
	g_object_unref(pixbuf);


	/* ---- load token file */
	filename = g_strconcat(PACKAGE_DATA_DIR "/", data->pngfile_token_filename, NULL);
	pixbuf = gdk_pixbuf_new_from_file(filename, &err);
	if (!pixbuf) {
		ret = -1;
		goto free_and_return;
	}

	for (i = 0 ; data->pngfile_token_width[i] ; i++) {
		data->pngfile_token_buf[i] = pixbuf_crop(pixbuf, data->pngfile_token_x[i], data->pngfile_token_y[i], data->pngfile_token_width[i], data->pngfile_token_height[i]);
	}

	g_free(filename);
	g_object_unref(pixbuf);


	/* ---- load star file */
	filename = g_strconcat(PACKAGE_DATA_DIR "/", data->pngfile_star_filename, NULL);
	pixbuf = gdk_pixbuf_new_from_file(filename, &err);
	if (!pixbuf) {
		ret = -1;
		goto free_and_return;
	}

	for (i = 0 ; data->pngfile_star_width[i] ; i++) {
		data->pngfile_star_buf[i] = pixbuf_crop(pixbuf, data->pngfile_star_x[i], data->pngfile_star_y[i], data->pngfile_star_width[i], data->pngfile_star_height[i]);
	}

	g_free(filename);
	g_object_unref(pixbuf);


	/* ---- load star_m file (mortgaged) */
	filename = g_strconcat(PACKAGE_DATA_DIR "/", data->pngfile_star_m_filename, NULL);
	pixbuf = gdk_pixbuf_new_from_file(filename, &err);
	if (!pixbuf) {
		ret = -1;
		goto free_and_return;
	}

	for (i = 0 ; data->pngfile_star_m_width[i] ; i++) {
		data->pngfile_star_m_buf[i] = pixbuf_crop(pixbuf, data->pngfile_star_m_x[i], data->pngfile_star_m_y[i], data->pngfile_star_m_width[i], data->pngfile_star_m_height[i]);
	}

	g_free(filename);
	g_object_unref(pixbuf);


	/* ---- load cards file */
	filename = g_strconcat(PACKAGE_DATA_DIR "/", data->pngfile_card_filename, NULL);
	pixbuf = gdk_pixbuf_new_from_file(filename, &err);
	if (!pixbuf) {
		ret = -1;
		goto free_and_return;
	}

	for (i = 0 ; data->pngfile_card_width[i] ; i++) {

		data->pngfile_card_buf[i] = pixbuf_crop(pixbuf, data->pngfile_card_x[i], data->pngfile_card_y[i], data->pngfile_card_width[i], data->pngfile_card_height[i]);
	}

	g_free(filename);
	g_object_unref(pixbuf);


	/* ---- load command file */
	filename = g_strconcat(PACKAGE_DATA_DIR "/", data->pngfile_command_filename, NULL);
	pixbuf = gdk_pixbuf_new_from_file(filename, &err);
	if (!pixbuf) {
		ret = -1;
		goto free_and_return;
	}

	for (i = 0 ; data->pngfile_command_width[i] ; i++) {
		data->pngfile_command_buf[i] = pixbuf_crop(pixbuf, data->pngfile_command_x[i], data->pngfile_command_y[i], data->pngfile_command_width[i], data->pngfile_command_height[i]);
	}

	g_free(filename);
	g_object_unref(pixbuf);


	/* ---- load horiz_houses file */
	filename = g_strconcat(PACKAGE_DATA_DIR "/", data->pngfile_horiz_house_filename, NULL);
	pixbuf = gdk_pixbuf_new_from_file(filename, &err);
	if (!pixbuf) {
		ret = -1;
		goto free_and_return;
	}

	for (i = 0 ; data->pngfile_horiz_house_width[i] ; i++) {
		data->pngfile_horiz_house_buf[i] = pixbuf_crop(pixbuf, data->pngfile_horiz_house_x[i], data->pngfile_horiz_house_y[i], data->pngfile_horiz_house_width[i], data->pngfile_horiz_house_height[i]);
	}

	g_free(filename);
	g_object_unref(pixbuf);


	/* ---- load vert_houses file */
	filename = g_strconcat(PACKAGE_DATA_DIR "/", data->pngfile_vert_house_filename, NULL);
	pixbuf = gdk_pixbuf_new_from_file(filename, &err);
	if (!pixbuf) {
		ret = -1;
		goto free_and_return;
	}

	for (i = 0 ; data->pngfile_vert_house_width[i] ; i++) {
		data->pngfile_vert_house_buf[i] = pixbuf_crop(pixbuf, data->pngfile_vert_house_x[i], data->pngfile_vert_house_y[i], data->pngfile_vert_house_width[i], data->pngfile_vert_house_height[i]);
	}

	data->png_game_loaded = TRUE;

free_and_return:
	if (err) {
		fprintf(stderr, "error while loading %s: %s\n", filename, err ? err->message : "(unknown)");
		g_error_free(err);
	}
	if (pixbuf)
		g_object_unref(pixbuf);
	if(filename) g_free(filename);
	return ret;
}


/* free all game pngs images */
void game_free_pngs()  {

	guint32 i;

	if(! data->png_game_loaded)  return;

	for(i = 0 ; data->pngfile_board_width[i] ; i++)  {
		if(data->pngfile_board_buf[i])  {
			g_object_unref(data->pngfile_board_buf[i]);
			data->pngfile_board_buf[i] = NULL;
		}
	}

	for(i = 0 ; data->pngfile_token_width[i] ; i++)  {
		if(data->pngfile_token_buf[i])  {
			g_object_unref(data->pngfile_token_buf[i]);
			data->pngfile_token_buf[i] = NULL;
		}
	}

	for(i = 0 ; data->pngfile_star_width[i] ; i++)  {
		if(data->pngfile_star_buf[i])  {
			g_object_unref(data->pngfile_star_buf[i]);
			data->pngfile_star_buf[i] = NULL;
		}
	}

	for(i = 0 ; data->pngfile_star_m_width[i] ; i++)  {
		if(data->pngfile_star_m_buf[i])  {
			g_object_unref(data->pngfile_star_m_buf[i]);
			data->pngfile_star_m_buf[i] = NULL;
		}
	}

	for(i = 0 ; data->pngfile_card_width[i] ; i++)  {
		if(data->pngfile_card_buf[i])  {
			g_object_unref(data->pngfile_card_buf[i]);
			data->pngfile_card_buf[i] = NULL;
		}
	}

	for(i = 0 ; data->pngfile_command_width[i] ; i++)  {
		if(data->pngfile_command_buf[i])  {
			g_object_unref(data->pngfile_command_buf[i]);
			data->pngfile_command_buf[i] = NULL;
		}
	}

	for(i = 0 ; data->pngfile_horiz_house_width[i] ; i++)  {
		if(data->pngfile_horiz_house_buf[i])  {
			g_object_unref(data->pngfile_horiz_house_buf[i]);
			data->pngfile_horiz_house_buf[i] = NULL;
		}
	}

	for(i = 0 ; data->pngfile_vert_house_width[i] ; i++)  {
		if(data->pngfile_vert_house_buf[i])  {
			g_object_unref(data->pngfile_vert_house_buf[i]);
			data->pngfile_vert_house_buf[i] = NULL;
		}
	}

	data->png_game_loaded = FALSE;
}

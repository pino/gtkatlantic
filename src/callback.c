/*
 *     gtkatlantic - the gtk+ monopd client, enjoy network monopoly games
 *
 *
 *  Copyright © 2002-2015 Sylvain Rochet
 *
 *  gtkatlantic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; see the file COPYING. If not, see
 *  <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <string.h>

#include "global.h"
#include "callback.h"
#include "client.h"
#include "game.h"
#include "interface.h"
#include "trade.h"
#include "load.h"
#include "theme.h"
#include "display.h"

#include "engine.h"


/* **** Show Configuration Panel */
void Callback_ShowConfiguration(GtkWidget* menuitem)  {
	(void)menuitem;

	interface_create_config_panel();
}


/* **** Get games from all monopd server */
void Callback_ConnectMetaServer(GtkWidget* button)  {
	(void)button;

	if (global->phase < PHASE_GAMECREATE) {
		if (global->phase < PHASE_GETGAMES) {
			interface_create_getgamespage(false);
		}
		create_connection_metaserver();
	}
}


/* get games: event "row_select" */
void Callback_GetGame_Select(GtkTreeSelection *selection, gpointer data)  {
	GtkTreeModel *model;
	gint32 gameid, canbejoined, canbewatched;
	(void)data;

	GtkWidget *button = g_object_get_data(G_OBJECT(global->MainVerticalBox), "connect_button");

	if (!gtk_tree_selection_get_selected(selection, &model, &global->selected_game)) {
		if (button) {
			gtk_button_set_label(GTK_BUTTON(button), "Select a game");
			gtk_widget_set_sensitive(button, FALSE);
		}
		return;
	}

	gtk_tree_model_get(model, &global->selected_game, GAMELIST_COLUMN_GAMEID, &gameid, GAMELIST_COLUMN_CANBEJOINED, &canbejoined, GAMELIST_COLUMN_CANBEWATCHED, &canbewatched, -1);

	if (button) {
		if (gameid < 0) {
			gtk_button_set_label(GTK_BUTTON(button), "Create game");
			gtk_widget_set_sensitive(button, TRUE);
		} else {
			if (canbejoined) {
				gtk_button_set_label(GTK_BUTTON(button), "Join game");
				gtk_widget_set_sensitive(button, TRUE);
			} else if (canbewatched) {
				gtk_button_set_label(GTK_BUTTON(button), "Watch game");
				gtk_widget_set_sensitive(button, TRUE);
			} else {
				gtk_button_set_label(GTK_BUTTON(button), "This game does not allow spectators");
				gtk_widget_set_sensitive(button, FALSE);
			}
		}
	}
}


/* **** Join/create a game */
void Callback_ConnectButton(GtkWidget* button)  {
	(void)button;
	gchar *cmd;
	gchar *host, *gametype;
	gint32 port, gameid, canbejoined, canbewatched;

	gtk_tree_model_get(GTK_TREE_MODEL(global->game_store), &global->selected_game, GAMELIST_COLUMN_HOST, &host, GAMELIST_COLUMN_PORT, &port, GAMELIST_COLUMN_GAMETYPE, &gametype, GAMELIST_COLUMN_GAMEID, &gameid,
		   GAMELIST_COLUMN_CANBEJOINED, &canbejoined, GAMELIST_COLUMN_CANBEWATCHED, &canbewatched, -1);

	if (! (gameid < 0 || canbejoined || canbewatched)) {
		return;
	}

	/* create a game */
	if (gameid < 0)  {
		cmd = g_strdup_printf(".n%s\n.gn%s\n", config->nickname, gametype);
	}
	/* join a game */
	else if (canbejoined) {
		cmd = g_strdup_printf(".n%s\n.gj%d\n", config->nickname, gameid);
	}
	/* watch a game */
	else {
		cmd = g_strdup_printf(".n%s\n.gS%d\n", config->nickname, gameid);
	}

	client_connect(CONNECT_TYPE_MONOPD_GAME, host, port, cmd);
}


/* **** Start the game :) */
void Callback_StartButton(GtkWidget* button)  {
	(void)button;

	client_send(global->game_connect, ".gs\n");
}


/* *** send data to server */
void Callback_SendButton(GtkWidget* Button, GtkWidget *entry) {
	(void)Button;

	gchar *tmp, *tmp2;

	tmp = g_strdup( gtk_entry_get_text( GTK_ENTRY(entry) ) );
	g_strstrip(tmp);

	if(tmp[0] != '/')   {

		tmp2 = g_strconcat(tmp, "\n", NULL);
		g_free(tmp);
		tmp = tmp2;
		client_send(global->game_connect, tmp);
	}
	else
		parse_specific_send_message(tmp);

	g_free(tmp);
	gtk_entry_set_text(GTK_ENTRY(entry), "");
}


/* *** save and apply the config */
void CallBack_ok_button_configwindow(GtkWidget* widget)  {
	(void)widget;

	gchar *filename, *errormsg;
	FILE *file;

	if (!global->path_home) {
		return;
	}
	filename = g_strconcat(global->path_home, "/gtkatlantic.conf", NULL);

	file = fopen(filename, "wt");
	if (!file) {
		errormsg = g_strdup_printf("Can't write config file at \"%s\"", filename);
		interface_set_infolabel(errormsg, "b00000", false);
		g_free(errormsg);
		g_free(filename);
		return;
	}
	g_free(filename);

	fprintf(file, "player nickname=\"%s\"\n",
		gtk_entry_get_text( GTK_ENTRY( g_object_get_data(G_OBJECT(global->ConfigWin), "config_player_nickname") ) )  );
	fprintf(file, "metaserver autoconnect=\"%d\" sendclientversion=\"%d\"\n",
		gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( g_object_get_data(G_OBJECT(global->ConfigWin), "config_metaserver_autoconnect") ) ),
		gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( g_object_get_data(G_OBJECT(global->ConfigWin), "config_metaserver_sendclientversion") ) )  );
	fprintf(file, "customserver host=\"%s\" port=\"%d\" autoconnect=\"%d\"\n",
		gtk_entry_get_text( GTK_ENTRY( g_object_get_data(G_OBJECT(global->ConfigWin), "config_customserver_host") ) ),
		atoi( gtk_entry_get_text( GTK_ENTRY( g_object_get_data(G_OBJECT(global->ConfigWin), "config_customserver_port") ) ) ),
		gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( g_object_get_data(G_OBJECT(global->ConfigWin), "config_customserver_autoconnect") ) )  );
	fprintf(file, "interface playerlist=\"%s\" token_speed=\"%d\" token_transparency=\"%d\"\n",
		gtk_combo_box_text_get_active_text( GTK_COMBO_BOX_TEXT( g_object_get_data(G_OBJECT(global->ConfigWin), "config_gameinterface_playerlistposition") ) ),
		(guint32)gtk_adjustment_get_value(GTK_ADJUSTMENT( g_object_get_data(G_OBJECT(global->ConfigWin), "config_gameinterface_tokenanimatespeed") ) ),
		gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( g_object_get_data(G_OBJECT(global->ConfigWin), "config_gameinterface_tokentransparency") ) )  );

	fclose(file);

	read_config_files();

	gtk_widget_destroy(global->ConfigWin);
}


/* *** reallow show config panel */
void CallBack_configwindow_destroyed(GtkWidget* widget)  {
	(void)widget;

	global->ConfigWin = 0;
}


/* *** button command pressed */
void CallBack_button_command_pressed(GtkWidget* button) {

	gchar *ptr = g_object_get_data(G_OBJECT(button), "command");
	client_send(global->game_connect, ptr);
}


/* *** button command pressed */
void Callback_AuctionButton_Absolute(GtkWidget* button, GtkWidget* entry)  {
	gchar *tosend;
	gint32 auction, money;

	auction = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(button), "auctionid"));
	money = atoi(gtk_entry_get_text(GTK_ENTRY(entry)));
	gtk_entry_set_text(GTK_ENTRY(entry), "");

	tosend = g_strdup_printf(".ab%d:%d\n", auction, money);
	client_send(global->game_connect, tosend);
	g_free(tosend);
}


/* *** button command pressed */
void Callback_AuctionButton_Relative(GtkWidget* button)  {
	gchar *tosend;
	gint32 auction, money;

	auction = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(button), "auctionid"));
	money = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(currentgame->BoardCenter), "auction_highbid"))
		+ GPOINTER_TO_INT(g_object_get_data(G_OBJECT(button), "value_add"));

	tosend = g_strdup_printf(".ab%d:%d\n", auction, money);
	client_send(global->game_connect, tosend);
	g_free(tosend);
}


/* *** callback clic on estate */
void Callback_ClicOnEstate(GtkWidget* widget, GdkEvent *event)  {

	if (event->type != GDK_BUTTON_PRESS) {
		return;
	}

	GtkWidget *menu = g_object_get_data(G_OBJECT(widget), "menu");
#if GTK_CHECK_VERSION(3,22,0)
	gtk_menu_popup_at_pointer(GTK_MENU(menu), NULL);
#else /* GTK_CHECK_VERSION(3,22,0) */
	GdkEventButton *button_event = (GdkEventButton *)event;
	gtk_menu_popup(GTK_MENU(menu), NULL, NULL, NULL, NULL, button_event->button, button_event->time);
#endif /* GTK_CHECK_VERSION(3,22,0) */
}


/* *** callback clic on estate */
void CallBack_PopupMenuEstate(GtkWidget* widget)  {
	guint32 id = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(gtk_widget_get_parent(widget)), "estateid"));
	gint32 action = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(widget), "action"));
	gchar *cmd = NULL;

	switch (action) {
		case ESTATE_ACTION_BUILDHOUSE:
			cmd = g_strdup_printf(".hb%d\n", id);
			break;
		case ESTATE_ACTION_SELLHOUSE:
			cmd = g_strdup_printf(".hs%d\n", id);
			break;
		case ESTATE_ACTION_MORTGAGE:
			cmd = g_strdup_printf(".em%d\n", id);
			break;
		case ESTATE_ACTION_SELL:
			cmd = g_strdup_printf(".es%d\n", id);
			break;
	}

	if (cmd) {
		client_send(global->game_connect, cmd);
		g_free(cmd);
	}
}


/* *** callback enter an estate */
void Callback_EnterAnEstate(GtkWidget* widget)  {

	gint32 id = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(widget), "estateid"));

	eng_obj *pic = currentgame->estate[id].pic;

	eng_pic_set_bgcolor(pic, 0x000000);
	eng_pic_set_alpha(pic, 170);
	update_display();
}


/* *** callback leave an estate */
void Callback_LeaveAnEstate(GtkWidget* widget)  {

	gint32 id = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(widget), "estateid"));

	eng_obj *pic = currentgame->estate[id].pic;
	eng_pic_unset_alpha(pic);
	update_display();
}


/* *** callback clic on player */
void Callback_ClicOnPlayer(GtkWidget* widget, GdkEvent *event)  {

	if (event->type != GDK_BUTTON_PRESS) {
		return;
	}

	GtkWidget *menu = g_object_get_data(G_OBJECT(widget), "menu");
#if GTK_CHECK_VERSION(3,22,0)
	gtk_menu_popup_at_pointer(GTK_MENU(menu), NULL);
#else /* GTK_CHECK_VERSION(3,22,0) */
	GdkEventButton *button_event = (GdkEventButton *)event;
	gtk_menu_popup(GTK_MENU(menu), NULL, NULL, NULL, NULL, button_event->button, button_event->time);
#endif /* GTK_CHECK_VERSION(3,22,0) */
}


/* *** callback clic on player */
void CallBack_PopupMenuPlayer(GtkWidget* widget)  {
	player *p = g_object_get_data(G_OBJECT(gtk_widget_get_parent(widget)), "player");
	gint32 action = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(widget), "action"));
	gchar *cmd = NULL;

	switch (action) {
		case PLAYER_ACTION_TRADE:
			cmd = g_strdup_printf(".Tn%d\n", p->playerid);
			break;
		case PLAYER_ACTION_VERSION:
			cmd = g_strdup_printf("!version %s\n", p->name);
			break;
		case PLAYER_ACTION_DATE:
			cmd = g_strdup_printf("!date %s\n", p->name);
			break;
		case PLAYER_ACTION_PING:
			cmd = g_strdup_printf("!ping %s\n", p->name);
			break;
	}

	if (cmd) {
		client_send(global->game_connect, cmd);
		g_free(cmd);
	}
}


/* *** callback for game option type bool */
void Callback_toggle_boolean_gameoption(GtkWidget* widget)  {
	gchar *text, *cmd;
	gint32 id;

	/* commands using the new configupdate protocol */
	id = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(widget), "id"));
	if (id > 0) {
		text = g_strdup_printf(".gc%d:%d\n", id, gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(widget) )  );
		client_send(global->game_connect, text);
		g_free(text);
		return;
	}

	/* commands using the old configupdate protocol */
	cmd = g_object_get_data(G_OBJECT(widget), "command");
	if (cmd) {
		text = g_strdup_printf("%s%d\n", cmd, gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(widget) )  );
		client_send(global->game_connect, text);
		g_free(text);
	}
}


/* *** callback for the entry menu connect to server */
void CallBack_menu_connect_to_server(GtkWidget* widget) {
	(void)widget;

	if (global->phase < PHASE_GAMECREATE)
		interface_create_connectoserver();
}


/* *** callback for the connect to server menu entry */
void CallBack_response_connect_to_server_window(GtkWidget* widget, gint response, GtkWidget *Win) {
	(void)widget;

	if (global->phase < PHASE_GAMECREATE && response == GTK_RESPONSE_OK) {
		GtkWidget *host = g_object_get_data(G_OBJECT(Win), "host");
		GtkWidget *port = g_object_get_data(G_OBJECT(Win), "port");

		if (global->phase < PHASE_GETGAMES) {
			interface_create_getgamespage(false);
		}

		create_connection_get_games((gchar*)gtk_entry_get_text(GTK_ENTRY(host)),
			atoi(gtk_entry_get_text(GTK_ENTRY(port))));
	}

	gtk_widget_destroy(Win);
}


/* *** callback for the entry menu connect to server */
void CallBack_menu_public_server_list(GtkWidget* widget) {
	(void)widget;

	if (global->phase == PHASE_GETGAMES)
		interface_create_publicserverlist();
}


/* *** callback for the entry menu change nickname */
void CallBack_menu_change_nickname(GtkWidget* widget)  {
	(void)widget;

	if(global->phase >= PHASE_GAMECREATE)
		interface_create_nicknamewin();
}


/* *** callback for the ok button of window change nickname */
void CallBack_response_changenick_window(GtkWidget* widget, gint response, GtkWidget *Win) {
	(void)widget;

	if(response == GTK_RESPONSE_OK)
	{
		GtkWidget *Entry = g_object_get_data(G_OBJECT(Win), "nickname");
		gchar *nickname, *sendstr;

		nickname = g_strdup( gtk_entry_get_text(GTK_ENTRY( Entry ) )  );

		if (strcmp(player_from_id(global->my_playerid)->name, nickname)) {
			sendstr = g_strdup_printf(".n%s\n", nickname);
			client_send(global->game_connect, sendstr);
			g_free(sendstr);
		}

		g_free(nickname);
	}

	gtk_widget_destroy(Win);
}


/* *** callback for the entry menu reconnect */
void CallBack_menu_reconnect(GtkWidget* widget)  {
	(void)widget;
	gchar buf[64];
	gchar *filename, *host = NULL, *cmd;
	guint32 port;
	FILE *file;
	size_t len;

	if (global->phase > PHASE_GETGAMES) {
		return;
	}

	if (!global->path_home) {
		return;
	}

	/* read cookie file */
	filename = g_strconcat(global->path_home, "/cookie", NULL);
	file = fopen(filename, "rt");
	g_free(filename);

	if (!file) {
		return;
	}

	if (fgets(buf, sizeof(buf), file) == NULL) {
		goto free_and_return;
	}
	len = strlen(buf);
	if (buf[len-1] != '\n') {
		goto free_and_return;
	}
	buf[len-1] = '\0';
	host = g_strdup(buf);

	if (fgets(buf, sizeof(buf), file) == NULL) {
		goto free_and_return;
	}
	len = strlen(buf);
	if (buf[len-1] != '\n') {
		goto free_and_return;
	}
	buf[len-1] = '\0';
	port = atoi(buf);

	if (fgets(buf, sizeof(buf), file) == NULL) {
		goto free_and_return;
	}
	len = strlen(buf);
	if (buf[len-1] != '\n') {
		goto free_and_return;
	}
	buf[len-1] = '\0';
	cmd = g_strdup_printf(".R%s\n", buf);

	/* rejoin game */
	client_connect(CONNECT_TYPE_MONOPD_GAME, host, port, cmd);

free_and_return:
	fclose(file);
	if (host) g_free(host);
}


void CallBack_menu_AddPlayer(GtkWidget *widget) {
	player *p = g_object_get_data(G_OBJECT(widget), "player");
	if (!p) {
		return;
	}

	gchar *cmd = g_strdup_printf(".gu%d\n", p->playerid);
	client_send(global->game_connect, cmd);
	g_free(cmd);
}


/* *** callback for all trades buttons */
void CallBack_trade_button(GtkWidget *button)  {

	gchar *sendstr = 0;
	gint32 command = GPOINTER_TO_INT(g_object_get_data( G_OBJECT(button), "command" ));
	guint32 tradeid = GPOINTER_TO_INT( g_object_get_data( G_OBJECT(button), "tradeid" )  );
	guint32	tradeslot = get_trade_slot_with_tradeid(tradeid);

	switch(command) {
		case TRADE_ACTION_REJECT:
			sendstr = g_strdup_printf(".Tr%d\n", tradeid);
			client_send(global->game_connect, sendstr);
			g_free(sendstr);
			break;
			
		case TRADE_ACTION_ACCEPT:
			sendstr = g_strdup_printf(".Ta%d:%d\n", tradeid, currentgame->trade[tradeslot].revision);
			client_send(global->game_connect, sendstr);
			g_free(sendstr);
			break;

		case TRADE_ACTION_REMOVE:
			if(currentgame->trade[tradeslot].select_type == TRADE_TYPE_MONEY)
				sendstr = g_strdup_printf(".Tm%d:%d:%d:%d\n", tradeid, currentgame->trade[tradeslot].select_from, currentgame->trade[tradeslot].select_to, 0);

			if(currentgame->trade[tradeslot].select_type == TRADE_TYPE_ESTATE)
				sendstr = g_strdup_printf(".Te%d:%d:%d\n", tradeid, currentgame->trade[tradeslot].select_aux, currentgame->trade[tradeslot].select_from);

			if(currentgame->trade[tradeslot].select_type == TRADE_TYPE_CARD)
				sendstr = g_strdup_printf(".Tc%d:%d:%d\n", tradeid, currentgame->trade[tradeslot].select_aux, currentgame->trade[tradeslot].select_from);

			if(sendstr)  {
				client_send(global->game_connect, sendstr);
				g_free(sendstr);
			}
			break;
	}
}


/* *** callback for all trade component */
void CallBack_trade_sub_component(GtkWidget *combo)  {

	gchar *text;
	gint32 tradeslot;
	
	tradeslot = get_trade_slot_with_tradeid( GPOINTER_TO_INT( g_object_get_data( G_OBJECT(combo), "tradeid" )  )  );
	if(tradeslot < 0) return;

	text = gtk_combo_box_text_get_active_text( GTK_COMBO_BOX_TEXT(combo) );
	if( !strcmp(text, "Money")) {
		currentgame->trade[tradeslot].current_component = TRADE_TYPE_MONEY;
	} else if( !strcmp(text, "Estate")) {
		currentgame->trade[tradeslot].current_component = TRADE_TYPE_ESTATE;
	} else {
		currentgame->trade[tradeslot].current_component = TRADE_TYPE_CARD;
	}

	trade_rebuild_subcomponent(tradeslot);
}


/* *** callback for all update component */
void CallBack_trade_update_component(GtkWidget *button)  {

	gchar *sendstr;
	guint32 tradeid, estateid, cardid, tradeslot, money;
	player *target, *from, *to;

	tradeslot = get_trade_slot_with_tradeid( GPOINTER_TO_INT( g_object_get_data( G_OBJECT(button), "tradeid" )  )  );
	tradeid = currentgame->trade[tradeslot].tradeid;

	switch(currentgame->trade[tradeslot].current_component)  {

		case TRADE_TYPE_ESTATE:

			estateid = get_estateid_by_estatename( (gchar*)gtk_combo_box_text_get_active_text( GTK_COMBO_BOX_TEXT(g_object_get_data( G_OBJECT(button), "name_estate" ) ))  );
			target = player_from_name((gchar*)gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(g_object_get_data(G_OBJECT(button), "name_target_player"))));

			sendstr = g_strdup_printf(".Te%d:%d:%d\n", tradeid, estateid, target->playerid);
			client_send(global->game_connect, sendstr);
			g_free(sendstr);
			break;

		case TRADE_TYPE_MONEY:

			from = player_from_name((gchar*)gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(g_object_get_data(G_OBJECT(button), "name_from_player"))));
			to = player_from_name((gchar*)gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(g_object_get_data(G_OBJECT(button), "name_to_player"))));
			money = atoi( gtk_entry_get_text( g_object_get_data( G_OBJECT(button), "amount_money" ) )  );

			sendstr = g_strdup_printf(".Tm%d:%d:%d:%d\n", tradeid, from->playerid, to->playerid, money);
			client_send(global->game_connect, sendstr);
			g_free(sendstr);
			break;

		case TRADE_TYPE_CARD:

			cardid = atoi( gtk_combo_box_text_get_active_text( GTK_COMBO_BOX_TEXT(g_object_get_data( G_OBJECT(button), "name_card" ) ))  );
			target = player_from_name((gchar*)gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(g_object_get_data(G_OBJECT(button), "name_target_player"))));
			sendstr = g_strdup_printf(".Tc%d:%d:%d\n", tradeid, cardid, target->playerid);
			client_send(global->game_connect, sendstr);
			g_free(sendstr);
			break;
	}
}


void CallBack_get_games_page_destroyed(GtkWidget *widget)  {
	(void)widget;

	g_object_unref(global->server_store);
	global->game_store = NULL;
	global->server_store = NULL;
}


/* *** callback for the entry menu leave game */
void CallBack_menu_LeaveGame(GtkWidget* widget)  {
	(void)widget;

	if (global->phase < PHASE_GAMECREATE) {
		return;
	}

	client_send(global->game_connect, ".gx\n");
	game_quit();
}


/* *** Show estates tree */
void CallBack_estates_tree(GtkWidget* widget) {
	(void)widget;

	if(global->phase != PHASE_GAMEPLAY) return;

	interface_create_estates_tree();
}




void CallBack_EstateTree_SelectRow(GtkTreeSelection *selection, gpointer data)  {
	GtkTreeIter iter;
	GtkTreeModel *model;
	gint32 id = -1;
	GtkWidget *parent, *info, *Label;
	gchar *Text;

	(void)data;

	if (!gtk_tree_selection_get_selected(selection, &model, &iter)) {
		return;
	}

	parent = g_object_get_data(G_OBJECT(currentgame->WinEstateTree), "parent_box");
	info = g_object_get_data(G_OBJECT(currentgame->WinEstateTree), "info_box");

	if(info) gtk_widget_destroy(info);
	/* info box */
	info = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	g_object_set_data(G_OBJECT(currentgame->WinEstateTree), "info_box", info);
	gtk_container_set_border_width(GTK_CONTAINER(info), 10);
	gtk_box_pack_start(GTK_BOX(parent), info, TRUE, TRUE, 0);

	gtk_tree_model_get(model, &iter, 1, &id, -1);
	if(id < 0) {

		/* info box */
		info = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
		g_object_set_data(G_OBJECT(currentgame->WinEstateTree), "info_box", info);
		gtk_container_set_border_width(GTK_CONTAINER(info), 10);
		gtk_box_pack_start(GTK_BOX(parent), info, TRUE, TRUE, 0);

		/* default text */
		Label = gtk_label_new("Select an estate...");
		gtk_widget_set_halign(Label, GTK_ALIGN_START);
		gtk_widget_set_valign(Label, GTK_ALIGN_START);
		gtk_box_pack_start(GTK_BOX(info), Label, FALSE, TRUE, 0);

		gtk_widget_show_all(info);
		return;
	}

	/* name */
	Text = g_strdup_printf("%s", currentgame->estate[id].name);

	Label = gtk_label_new(Text);
	gtk_widget_set_halign(Label, GTK_ALIGN_START);
	gtk_widget_set_valign(Label, GTK_ALIGN_START);
	gtk_box_pack_start(GTK_BOX(info), Label, FALSE, TRUE, 0);

	g_free(Text);


	/* group */
	if(currentgame->estate[id].group >= 0)  Text = g_strdup_printf("%s\n", currentgame->group[ currentgame->estate[id].group ].name);
	else  Text = g_strdup("\n");

	Label = gtk_label_new(Text);
	gtk_widget_set_halign(Label, GTK_ALIGN_START);
	gtk_widget_set_valign(Label, GTK_ALIGN_START);
	gtk_box_pack_start(GTK_BOX(info), Label, FALSE, TRUE, 0);

	g_free(Text);

	/* state (for sale, owned, mortgaged, unknow?) */
	if(currentgame->estate[id].price)  {

		if(currentgame->estate[id].owner <= 0)  Text = g_strdup("State: for sale");
		else  if(currentgame->estate[id].owner > 0  &&  currentgame->estate[id].mortgaged)  Text = g_strdup("State: mortgaged");
		else  if(currentgame->estate[id].owner > 0)  Text = g_strdup("State: owned");
		else  Text = g_strdup("State: unknow");

		Label = gtk_label_new(Text);
		gtk_widget_set_halign(Label, GTK_ALIGN_START);
		gtk_widget_set_valign(Label, GTK_ALIGN_START);
		gtk_box_pack_start(GTK_BOX(info), Label, FALSE, TRUE, 0);

		g_free(Text);
	}

	/* owner */
	if(currentgame->estate[id].price)  {

		if(currentgame->estate[id].owner <= 0)  Text = g_strdup("Owner: no");
		else  Text = g_strdup_printf("Owner: %s", player_from_id(currentgame->estate[id].owner)->name);

		Label = gtk_label_new(Text);
		gtk_widget_set_halign(Label, GTK_ALIGN_START);
		gtk_widget_set_valign(Label, GTK_ALIGN_START);
		gtk_box_pack_start(GTK_BOX(info), Label, FALSE, TRUE, 0);

		g_free(Text);
	}

	/* price */
	if(currentgame->estate[id].price)  {

		Text = g_strdup_printf("\nPrice: %d", currentgame->estate[id].price);

		Label = gtk_label_new(Text);
		gtk_widget_set_halign(Label, GTK_ALIGN_START);
		gtk_widget_set_valign(Label, GTK_ALIGN_START);
		gtk_box_pack_start(GTK_BOX(info), Label, FALSE, TRUE, 0);

		g_free(Text);
	}

	/* money */
	if(currentgame->estate[id].money)  {

		Text = g_strdup_printf("Money: %d", currentgame->estate[id].money);

		Label = gtk_label_new(Text);
		gtk_widget_set_halign(Label, GTK_ALIGN_START);
		gtk_widget_set_valign(Label, GTK_ALIGN_START);
		gtk_box_pack_start(GTK_BOX(info), Label, FALSE, TRUE, 0);

		g_free(Text);
	}

	/* rent */
	if(currentgame->estate[id].rent[5])  {

		Text = g_strdup_printf("\nRent with no house: %d", currentgame->estate[id].rent[0]);

		Label = gtk_label_new(Text);
		gtk_widget_set_halign(Label, GTK_ALIGN_START);
		gtk_widget_set_valign(Label, GTK_ALIGN_START);
		gtk_box_pack_start(GTK_BOX(info), Label, FALSE, TRUE, 0);

		g_free(Text);


		Text = g_strdup_printf("Rent with 1 house: %d", currentgame->estate[id].rent[1]);

		Label = gtk_label_new(Text);
		gtk_widget_set_halign(Label, GTK_ALIGN_START);
		gtk_widget_set_valign(Label, GTK_ALIGN_START);
		gtk_box_pack_start(GTK_BOX(info), Label, FALSE, TRUE, 0);

		g_free(Text);


		Text = g_strdup_printf("Rent with 2 houses: %d", currentgame->estate[id].rent[2]);

		Label = gtk_label_new(Text);
		gtk_widget_set_halign(Label, GTK_ALIGN_START);
		gtk_widget_set_valign(Label, GTK_ALIGN_START);
		gtk_box_pack_start(GTK_BOX(info), Label, FALSE, TRUE, 0);

		g_free(Text);


		Text = g_strdup_printf("Rent with 3 houses: %d", currentgame->estate[id].rent[3]);

		Label = gtk_label_new(Text);
		gtk_widget_set_halign(Label, GTK_ALIGN_START);
		gtk_widget_set_valign(Label, GTK_ALIGN_START);
		gtk_box_pack_start(GTK_BOX(info), Label, FALSE, TRUE, 0);

		g_free(Text);


		Text = g_strdup_printf("Rent with 4 houses: %d", currentgame->estate[id].rent[4]);

		Label = gtk_label_new(Text);
		gtk_widget_set_halign(Label, GTK_ALIGN_START);
		gtk_widget_set_valign(Label, GTK_ALIGN_START);
		gtk_box_pack_start(GTK_BOX(info), Label, FALSE, TRUE, 0);

		g_free(Text);


		Text = g_strdup_printf("Rent with a hotel: %d", currentgame->estate[id].rent[5]);

		Label = gtk_label_new(Text);
		gtk_widget_set_halign(Label, GTK_ALIGN_START);
		gtk_widget_set_valign(Label, GTK_ALIGN_START);
		gtk_box_pack_start(GTK_BOX(info), Label, FALSE, TRUE, 0);

		g_free(Text);
	}

	/* houses prices */
	if(currentgame->estate[id].houseprice)  {

		Text = g_strdup_printf("\nBuy house price: %d", currentgame->estate[id].houseprice);

		Label = gtk_label_new(Text);
		gtk_widget_set_halign(Label, GTK_ALIGN_START);
		gtk_widget_set_valign(Label, GTK_ALIGN_START);
		gtk_box_pack_start(GTK_BOX(info), Label, FALSE, TRUE, 0);

		g_free(Text);
	}

	if(currentgame->estate[id].sellhouseprice)  {

		Text = g_strdup_printf("Sell house price: %d", currentgame->estate[id].sellhouseprice);

		Label = gtk_label_new(Text);
		gtk_widget_set_halign(Label, GTK_ALIGN_START);
		gtk_widget_set_valign(Label, GTK_ALIGN_START);
		gtk_box_pack_start(GTK_BOX(info), Label, FALSE, TRUE, 0);

		g_free(Text);
	}

	/* number of houses */
	if(currentgame->estate[id].houseprice)  {

		Text = g_strdup_printf("Number of houses: %d", currentgame->estate[id].houses);

		Label = gtk_label_new(Text);
		gtk_widget_set_halign(Label, GTK_ALIGN_START);
		gtk_widget_set_valign(Label, GTK_ALIGN_START);
		gtk_box_pack_start(GTK_BOX(info), Label, FALSE, TRUE, 0);

		g_free(Text);
	}

	/* (un)mortgage value */
	if(currentgame->estate[id].mortgageprice)  {

		Text = g_strdup_printf("\nMortgage price: %d", currentgame->estate[id].mortgageprice);

		Label = gtk_label_new(Text);
		gtk_widget_set_halign(Label, GTK_ALIGN_START);
		gtk_widget_set_valign(Label, GTK_ALIGN_START);
		gtk_box_pack_start(GTK_BOX(info), Label, FALSE, TRUE, 0);

		g_free(Text);
	}

	if(currentgame->estate[id].unmortgageprice)  {

		Text = g_strdup_printf("Unmortgage price: %d", currentgame->estate[id].unmortgageprice);

		Label = gtk_label_new(Text);
		gtk_widget_set_halign(Label, GTK_ALIGN_START);
		gtk_widget_set_valign(Label, GTK_ALIGN_START);
		gtk_box_pack_start(GTK_BOX(info), Label, FALSE, TRUE, 0);

		g_free(Text);
	}


	gtk_widget_show_all(info);
}


void CallBack_EstateTree_Destroyed(GtkWidget *widget)  {
	(void)widget;

	currentgame->WinEstateTree = 0;
}


/* proposal list: event "row_select" */
void Callback_ProposalList_Select(GtkTreeSelection *selection, gpointer data) {
	GtkTreeIter iter;
	GtkTreeModel *model;
	gint32 tradeid, tradeslot, typeid, fromid, toid, auxid;
	(void)data;

	tradeid = GPOINTER_TO_INT( g_object_get_data( G_OBJECT(selection), "tradeid" )  );
	tradeslot = get_trade_slot_with_tradeid(tradeid);

	if (!gtk_tree_selection_get_selected(selection, &model, &iter)) {
		/* Nothing selected */
		currentgame->trade[tradeslot].select_type = 0;
		currentgame->trade[tradeslot].select_from = 0;
		currentgame->trade[tradeslot].select_to = 0;
		currentgame->trade[tradeslot].select_aux = 0;
		return;
	}

	gtk_tree_model_get(model, &iter,
	  TRADEPROPOSALLIST_COLUMN_TYPE_ID, &typeid,
	  TRADEPROPOSALLIST_COLUMN_FROM_ID, &fromid,
	  TRADEPROPOSALLIST_COLUMN_TO_ID, &toid,
	  TRADEPROPOSALLIST_COLUMN_AUX_ID, &auxid,
	  -1);

	currentgame->trade[tradeslot].select_type = typeid;
	currentgame->trade[tradeslot].select_from = fromid;
	currentgame->trade[tradeslot].select_to = toid;
	currentgame->trade[tradeslot].select_aux = auxid;
}

void CallBack_HelpWin_Destroyed(GtkWidget *widget)  {
	(void)widget;

	global->HelpWin = 0;
}


void Callback_menu_Themes(GtkWidget *widget)  {
	(void)widget;

	if(global->ThemeWin)  return;

	theme_build_selection_win();
	theme_build_database();
	theme_fill_selection_list();
}


void CallBack_ThemeWin_Delete(GtkWidget *widget)  {
	(void)widget;

	theme_preview_free();
	gtk_widget_destroy(global->ThemeWin);
}


void CallBack_ThemeWin_Destroyed(GtkWidget *widget)  {
	(void)widget;

	global->ThemeWin = 0;
}


/* theme list: event "row_select" */
void Callback_ThemeList_Select(GtkTreeSelection *selection, gpointer data) {
	GtkTreeIter iter;
	GtkTreeModel *model;
	gint32 themeid;
	(void)data;

	if (!gtk_tree_selection_get_selected(selection, &model, &iter)) {
		return;
	}

	gtk_tree_model_get(model, &iter,
	  THEMELIST_COLUMN_ID, &themeid,
	  -1);

	theme_display(themeid);
}


/* select/apply a theme */
void CallBack_Theme_Apply(GtkWidget* button)  {
	(void)button;

	theme_load( atoi( g_object_get_data(G_OBJECT(global->ThemeWin), "theme_preview_selected") ) );
}

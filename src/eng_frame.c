/*
 *     gtkatlantic - the gtk+ monopd client, enjoy network monopoly games
 *
 *
 *  Copyright © 2002-2015 Sylvain Rochet
 *
 *  gtkatlantic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; see the file COPYING. If not, see
 *  <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <glib.h>
#include <string.h>

#include "engine.h"


/* ---- update all frame */
void eng_create_frame_all()  {

	eng_list_e *lst;

	for(lst = eng_list_first(eng->frame) ; lst ; lst=lst->next)  {

		eng_frame *f = lst->data;
		if(f && f->compute)
			eng_create_frame(lst->data);
	}
}




/* ---- update a frame */
void eng_create_frame(eng_frame *f)  {

	guint32 size = 0;
	eng_list *obj_valid, *obj_show, *obj_modif;
	eng_list_e *lst, *next;

	if(!f) return;

	f->x_min = MAX_WIDTH;
	f->y_min = MAX_HEIGHT;
	f->x_max = 0;
	f->y_max = 0;

	obj_valid = eng_list_new();
	obj_show = eng_list_new();
	obj_modif = eng_list_new();

	/* free zone_upd */
	eng_frame_free_zoneupd(f);

	/* allocate memory for output buffer */
	if(!f->memalloc)  {

		eng_coord z;

		size = f->width * f->height * 4;
		f->bufout = g_malloc(size);
		z.x1 = 0;
		z.y1 = 0;
		z.x2 = f->width -1;
		z.y2 = f->height -1;
		eng_clear_bufout(f, &z);
		f->memalloc = size;
	}

	/* create table of valid objects */
	for(lst = eng_list_first(f->obj) ; lst ; lst=lst->next) {

		eng_obj *o = lst->data;
	 	o->entry_valid = NULL;
		o->entry_show = NULL;
		o->entry_modif = NULL;
		if(eng_pic_test(o))
			o->entry_valid = eng_list_append(obj_valid, o);
	}

//	fprintf(stderr, "NB_OBJ_VALID  = %d\n", eng_list_length(obj_valid));

	/* create table of showed objects */
	for(lst = eng_list_first(obj_valid) ; lst ; lst=lst->next) {

		eng_obj *o = lst->data;
		if(o->show)
			o->entry_show = eng_list_append(obj_show, o);
	}

//	fprintf(stderr, "NB_OBJ_SHOW   = %d\n", eng_list_length(obj_show));

	/* create table of modified objects */
	for(lst = eng_list_first(obj_valid) ; lst ; lst=lst->next) {

		eng_obj *o = lst->data;
		if(o->change) {

			o->entry_modif = eng_list_append(obj_modif, o);
			o->change = 0;
		}
	}

//	fprintf(stderr, "NB_OBJ_MODIF  = %d\n", eng_list_length(obj_modif));


	/* search zones to update */
	for(next = NULL, lst = eng_list_first(obj_modif) ; lst ; lst = next ? next : eng_list_next(lst), next = NULL) {

		eng_coord *c;
		eng_obj *o = lst->data;

		/* get zones needed to update */
		c = eng_pic_get_zone_old(o);
		if(c->x2 > c->x1  &&  c->y2 > c->y1)
			c->entry = eng_list_append(f->zone_upd, c);
		else
			g_free(c);   /* just if old value is not available, new object */

		c = eng_pic_get_zone_new(o);
		c->entry = eng_list_append(f->zone_upd, c);

		/* destroy pic if requested */
		if(o->destroy)  {

			next = lst->next;
			eng_list_remove_fast(o->entry_valid);
			eng_list_remove_fast(o->entry_show);
			eng_list_remove_fast(o->entry_modif);
			eng_pic_free(o);
			lst = NULL;
			continue;
		}

		/* store value of x, y, width, height, they are used if image move */
		o->x_old = o->x;
		o->y_old = o->y;
		o->width_old = o->width;
		o->height_old = o->height;
	}
/*
	printf("NB_ZONE_MODIF = %d\n", eng_list_length(f->zone_upd));

	for(lst = eng_list_first(f->zone_upd) ; lst ; lst=lst->next)  {

		coord *c = lst->data;
		printf("   %d, %d - %d, %d \n", c->x1,  c->y1, c->x2, c->y2 );
	}
*/
	/* remove redundant zone & superposed zone */
	for(next = NULL, lst = eng_list_first(f->zone_upd) ; lst ; lst = next ? next : eng_list_next(lst), next = NULL) {

		eng_list_e *lst2;
		eng_coord *first = lst->data;

		for(lst2=lst->next ; lst2 ; lst2=lst2->next)  {

			eng_coord *second = lst2->data;

			if(   second->x1 <= first->x2  \
			  &&  second->x2 >= first->x1  \
			  &&  second->y1 <= first->y2  \
			  &&  second->y2 >= first->y1)  {

				if( second->x1 > first->x1 )
					second->x1 = first->x1;
				if( second->y1 > first->y1 )
					second->y1 = first->y1;
				if( second->x2 < first->x2 )
					second->x2 = first->x2;
				if( second->y2 < first->y2 )
					second->y2 = first->y2;

				next=lst->next;
				eng_list_remove_fast(first->entry);
				g_free(first);
				lst = NULL;
				break;
			}
		}
	}
/*
	printf("NB_ZONE_MODIF = %d\n", eng_list_length(f->zone_upd));

	for(lst = eng_list_first(f->zone_upd) ; lst ; lst=lst->next)  {

		coord *c = lst->data;
		printf("   %d, %d - %d, %d \n", c->x1,  c->y1, c->x2, c->y2 );
	}
*/

	/* update bufout, get extrem  */
	for(lst = eng_list_first(f->zone_upd) ; lst ; lst=lst->next)  {

		eng_coord *c = lst->data;

		/* extrem */
		if(c->x1 < f->x_min)
			f->x_min = c->x1;
		if(c->y1 < f->y_min)
			f->y_min = c->y1;
		if(c->x2 > f->x_max)
			f->x_max = c->x2;
		if(c->y2 > f->y_max)
			f->y_max = c->y2;

		/* update bufout */
		eng_update_bufout(f, c, obj_show);
	}

	if(!f->y_max) {

		f->x_min = 0;
		f->y_min = 0;
		f->x_max = 0;
		f->y_max = 0;
	}
/*
	printf("EXTREM: ");
	printf("   %d, %d - %d, %d \n", f->x_min,  f->y_min, f->x_max,  f->y_max );
*/

	eng_list_destroy(obj_valid);
	eng_list_destroy(obj_show);
	eng_list_destroy(obj_modif);
}




/* get old zone where the pic is displayed before change */
eng_coord* eng_pic_get_zone_old(eng_obj *o) {

	eng_coord *c;

	c = g_malloc(sizeof(eng_coord) );

	c->x1 = o->x_old;
	c->y1 = o->y_old;

	c->x2 = c->x1 + o->width_old -1;
	c->y2 = c->y1 + o->height_old -1;

	if(c->x1 < 0) c->x1 = 0;
	if(c->y1 < 0) c->y1 = 0;

	if(c->x2 >= o->frame->width) c->x2 = o->frame->width -1;
	if(c->y2 >= o->frame->height) c->y2 = o->frame->height -1;

	return(c);
}




/* get new zone where the pic is displayed after change */
eng_coord* eng_pic_get_zone_new(eng_obj *o) {

	eng_coord *c;

	c = g_malloc(sizeof(eng_coord) );

	c->x1 = o->x;
	c->y1 = o->y;

	c->x2 = c->x1 + o->width -1;
	c->y2 = c->y1 + o->height -1;

	if(c->x1 < 0) c->x1 = 0;
	if(c->y1 < 0) c->y1 = 0;

	if(c->x2 >= o->frame->width) c->x2 = o->frame->width -1;
	if(c->y2 >= o->frame->height) c->y2 = o->frame->height -1;

	return(c);
}




/* update bufout in selected zone */
void eng_update_bufout(eng_frame *f, eng_coord *zone, eng_list *obj_show)  {

	eng_list *obj_in_zone, *obj_in_zone_unsorted;
	eng_list_e *lst;

	obj_in_zone_unsorted = eng_list_new();
	eng_clear_bufout(f, zone);

	/* search obj in zone to update */
	for(lst = eng_list_first(obj_show) ; lst ; lst=lst->next) {

		eng_obj *o = lst->data;
		eng_coord *c = eng_pic_get_zone_new(o);

		/* obj superposed with zone to update ? */
		if(   c->x1 <= zone->x2
		  &&  c->x2 >= zone->x1
		  &&  c->y1 <= zone->y2
		  &&  c->y2 >= zone->y1)  {

			o->entry_zone = eng_list_append(obj_in_zone_unsorted, o);
		}
		g_free(c);
	}
/*
	printf("--- IN ZONE (UNSORTED) : %d, %d - %d, %d \n", zone->x1, zone->y1, zone->x2, zone->y2);
	for(lst = eng_list_first(obj_in_zone_unsorted) ; lst ; lst=lst->next) {

		eng_obj *o = lst->data;
		printf("    %3d = %3d, %3d, %3d, %3d\n", o->z, o->x, o->y, o->x + o->width -1, o->y + o->height -1);
	}
*/
	/* sort table : ground min > max */
	obj_in_zone = eng_list_new();
	while (eng_list_length(obj_in_zone_unsorted)) {
		eng_obj *a, *b;
		lst = eng_list_first(obj_in_zone_unsorted);
		a = lst->data;
		for (; lst; lst=lst->next) {
			b = lst->data;
			if (a->z > b->z) {
				a = b;
			}
		}
		eng_list_remove_fast(a->entry_zone);
		a->entry_zone = eng_list_append(obj_in_zone, a);
	}
	eng_list_destroy(obj_in_zone_unsorted);
/*
	printf("--- IN ZONE (SORT) : %d, %d - %d, %d \n", zone->x1, zone->y1, zone->x2, zone->y2);
	for(lst = eng_list_first(obj_in_zone) ; lst ; lst=lst->next) {

		eng_obj *o = lst->data;
		printf("    %3d = %3d, %3d, %3d, %3d\n", o->z, o->x, o->y, o->x + o->width -1, o->y + o->height -1);
	}
*/
	/* modify bufout */
	for(lst = eng_list_first(obj_in_zone) ; lst ; lst=lst->next) {
		guint16 x1_draw, y1_draw, x2_draw, y2_draw;
		guint16 height_draw, width_draw;
		guint32 ptr_memout, ptr_memobj;
		eng_obj *o = lst->data;
		eng_coord *c = eng_pic_get_zone_new(o);
		guint32 i, j;

		/* search part of pic to update */
		if(c->x1 <= zone->x1 ) x1_draw = zone->x1;
		else x1_draw = c->x1;

		if(c->y1 <= zone->y1 ) y1_draw = zone->y1;
		else y1_draw = c->y1;

		if(c->x2 >= zone->x2 ) x2_draw = zone->x2;
		else x2_draw = c->x2;

		if(c->y2 >= zone->y2 ) y2_draw = zone->y2;
		else y2_draw = c->y2;

//		printf("-> %3d, %3d, %3d, %3d\n", x1_draw, y1_draw, x2_draw, y2_draw);

		width_draw  = x2_draw - x1_draw +1;
		height_draw = y2_draw - y1_draw +1;

		ptr_memout = y1_draw * f->width + x1_draw;
		ptr_memobj = (y1_draw - c->y1) * o->width + x1_draw - c->x1;

		/* ---> rgba pic */
		for(i = 0 ; i < height_draw ; i ++)  {
			const guint8 *in;    /* PNG RGBA byte order */
			guint32 *out;  /* ARGB32, native-endian, premultiplied alpha, designed for CAIRO */
			in =
				/* gdk_pixbuf_read_pixels appeared in 2.32, we are using older libGDK-PixBuf version on Windows and Debian/kfreebsd */
#if GDK_PIXBUF_MAJOR > 2 || (GDK_PIXBUF_MAJOR == 2 && GDK_PIXBUF_MINOR >= 32)
				gdk_pixbuf_read_pixels(o->pixbuf)
#else /* GDK-PixBuf >= 2.32 */
				gdk_pixbuf_get_pixels(o->pixbuf)
#endif /* GDK-PixBuf >= 2.32 */
				+ ptr_memobj*4;
			out = (guint32*)(f->bufout + ptr_memout*4);

			for(j = 0 ; j < width_draw ; j++)  {
				guint8 ri, gi, bi, ai;

				ri = *in++; 
				gi = *in++;
				bi = *in++;
				ai = *in++;

				if (o->have_alpha) {
					ai = eng->alpha->fg[o->alpha][ai];
				}

				if (!o->have_bgcolor) {
					guint8 ro, go, bo, ao;

					ao = *out >> 24;
					ro = *out >> 16;
					go = *out >> 8;
					bo = *out;

					*out++ = ((eng->alpha->fg[ao][ao] + eng->alpha->bg[ao][ai]) << 24)
					  | ((eng->alpha->fg[ai][ri] + eng->alpha->bg[ai][ro]) << 16)
					  | ((eng->alpha->fg[ai][gi] + eng->alpha->bg[ai][go]) << 8)
					  | (eng->alpha->fg[ai][bi] + eng->alpha->bg[ai][bo]);
				} else {
					*out++ = (0xff << 24)
					  | ((eng->alpha->fg[ai][ri] + eng->alpha->bg[ai][o->bgcolor[0]]) << 16)
					  | ((eng->alpha->fg[ai][gi] + eng->alpha->bg[ai][o->bgcolor[1]]) << 8)
					  | (eng->alpha->fg[ai][bi] + eng->alpha->bg[ai][o->bgcolor[2]]);
				}
			}

			ptr_memout += f->width;
			ptr_memobj += o->width;
		}

		g_free(c);
	}

 	eng_list_destroy(obj_in_zone);
}



void eng_clear_bufout(eng_frame *f, eng_coord *zone)  {

	guint8 *out;
	guint32 i;
	guint32 height = zone->y2 - zone->y1 +1;
	guint32 width = (zone->x2 - zone->x1 +1) * 4;

	out = f->bufout + (zone->y1 * f->width  + zone->x1) * 4;
	for(i = 0 ; i < height ; i++) {
		memset(out, 0, width);
		out += f->width * 4;
	}
}




gboolean eng_zone_is_modified(eng_frame *f, eng_coord *zone)  {

	eng_list_e *lst;

	for(lst = eng_list_first(f->zone_upd) ; lst ; lst=lst->next)  {

		eng_coord *c = lst->data;

		if(   c->x1 <= zone->x2
		  &&  c->x2 >= zone->x1
		  &&  c->y1 <= zone->y2
		  &&  c->y2 >= zone->y1)  {

			return(TRUE);
		}
	}

	return(FALSE);
}



guchar *eng_get_zone(eng_frame *f, eng_coord *zone)  {

	guint32 i, rwidth, rheight;
	guchar *in, *buf, *out;

	if (zone->stride < zone->width*4
	  || zone->x >= f->width
	  || zone->y >= f->height) {
		return NULL;
	}

	if (zone->x + zone->width > f->width) {
		rwidth = f->width - zone->x;
	} else {
		rwidth = zone->width;
	}

	if (zone->y + zone->height > f->height) {
		rheight = f->height - zone->y;
	} else {
		rheight = zone->height;
	}

	buf = out = g_malloc0(zone->stride * zone->height);
	in = f->bufout + (zone->y * f->width + zone->x) * 4;
	for(i = 0 ; i < rheight ; i++) {
		memcpy(out, in, rwidth * 4);
		in += f->width * 4;
		out += zone->stride;
	}

	return(buf);
}



eng_zone *eng_get_next_zone(eng_frame *f) {

	eng_list_e *lst;
	eng_coord *c;
	eng_zone *z;

	lst = eng_list_first(f->zone_upd);
	if(!lst)  return NULL;

	c = lst->data;

	z = g_malloc0( sizeof(eng_zone) );
	z->aux = eng_get_zone(f, c);
	z->x1 = c->x1;
	z->y1 = c->y1;
	z->x2 = c->x2;
	z->y2 = c->y2;
	z->width = c->x2 - c->x1 +1;
	z->height = c->y2 - c->y1 +1;

	g_free(lst->data);
	eng_list_remove_fast(c->entry);

	return z;
}

/*
 *     gtkatlantic - the gtk+ monopd client, enjoy network monopoly games
 *
 *
 *  Copyright © 2002-2015 Sylvain Rochet
 *
 *  gtkatlantic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; see the file COPYING. If not, see
 *  <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <sys/time.h>
#include <gtk/gtk.h>

#include <libxml/parser.h>

#include "global.h"
#include "game.h"
#include "load.h"
#include "interface.h"
#include "client.h"
#include "trade.h"
#include "engine.h"
#include "callback.h"

#ifdef WIN32
#include <windows.h>
#include <io.h>
#include <shlobj.h>
#define mkdir(p, m) _mkdir(p)
#endif

/* create home directory (~/.gtkatlantic) *
 * return TRUE if exist/created, return FALSE if cannot be created */
void create_home_directory(void) {
	gchar *path;
	struct stat s;

#ifndef WIN32
	path = g_strconcat(getenv("HOME"), "/", ".", PACKAGE, NULL);
#else /* WIN32 */
	TCHAR home[MAX_PATH];
	if (FAILED(SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, 0, home))) {
		return;
	}
	path = g_strconcat(home, "/", PACKAGE, NULL);
#endif /* WIN32 */
	if (stat(path, &s) < 0) {
		mkdir(path, 0777);
		if (stat(path, &s) < 0) {
			g_free(path);
			return;
		}
	}

	global->path_home = path;

	path = g_strconcat(global->path_home, "/themes", NULL);
	if (stat(path, &s) < 0) {
		mkdir(path, 0777);
	}
	g_free(path);
}


/* read config files */
void read_config_files()   {

	gchar *filename;

	/* Default configuration */
	config->nickname = g_strdup("anonymous");

	config->metaserver_autoconnect = TRUE;
	config->metaserver_sendclientversion = TRUE;

	config->getgames_host = g_strdup("localhost");
	config->getgames_port = 1234;
	config->getgames_autoconnect = FALSE;

	config->game_playerlist_position = GAME_PLAYERLIST_POS_LEFT;
	config->game_token_animation_speed = 300;
	config->game_token_transparency = FALSE;

	/* read user config file */
	if (global->path_home) {
		filename = g_strconcat(global->path_home, "/gtkatlantic.conf", NULL);
		parse_file_config(filename);
		g_free(filename);
	}
}


/* this function made a new connection for metaserver (list & games) */
void create_connection_metaserver()  {
	GtkTreeIter iter;
	gboolean valid;
	gchar *cmd;

	/* already connected */
	if (global->metaserver_connect) {
		return;
	}

	/* remove all servers */
	gtk_list_store_clear(global->server_store);

	/* remove all games previously fetched from metaserver */
	valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(global->game_store), &iter);
	while (valid) {
		gint32 source;
		GtkTreeIter curiter = iter;

		valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(global->game_store), &iter); /* get next iter so we can safely remove the current entry */

		gtk_tree_model_get(GTK_TREE_MODEL(global->game_store), &curiter, GAMELIST_COLUMN_SOURCE, &source, -1);
		if (source == GAMELIST_SOURCE_METASERVER)  {
			gtk_list_store_remove(global->game_store, &curiter);
		}
	}

	GtkWidget *MenuItem = g_object_get_data(G_OBJECT(global->Menu), "connect_to_metaserver");
	gtk_widget_set_sensitive(MenuItem, FALSE);
	MenuItem = g_object_get_data(G_OBJECT(global->Menu), "public_server_list");
	gtk_widget_set_sensitive(MenuItem, TRUE);

	if (config->metaserver_sendclientversion) {
		cmd = g_strdup("CHECKCLIENT " PACKAGE " " VERSION "\nFOLLOW\n");
	} else {
		cmd = g_strdup("FOLLOW\n");
	}

	client_connect(CONNECT_TYPE_METASERVER, METASERVER_HOST, METASERVER_PORT, cmd);
}


/* this function create a new connection to get games list */
void create_connection_get_games(gchar *host, gint32 port)  {

	/* remove all games previously fetched from custom server */
	if (global->game_store) {
		GtkTreeIter iter;
		gboolean valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(global->game_store), &iter);
		while (valid) {
			gint32 source;
			GtkTreeIter curiter = iter;

			valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(global->game_store), &iter); /* get next iter so we can remove the current entry safely */

			gtk_tree_model_get(GTK_TREE_MODEL(global->game_store), &curiter, GAMELIST_COLUMN_SOURCE, &source, -1);
			if (source == GAMELIST_SOURCE_CUSTOM)  {
				gtk_list_store_remove(global->game_store, &curiter);
			}
		}
	}

	client_connect(CONNECT_TYPE_MONOPD_GETGAME, host, port, NULL);
}


/* insert test in Chat Box */
void text_insert_chat(gchar* text, gint32 length)
{
	GtkTextBuffer *textbuff;
	GtkTextIter textiter;
	GtkTextMark *textmark;

	if(global->phase < PHASE_GAMECREATE)
		return;

	textbuff = gtk_text_view_get_buffer(GTK_TEXT_VIEW(currentgame->Chat));
	gtk_text_buffer_get_end_iter(textbuff, &textiter);
	if (gtk_text_iter_get_offset(&textiter)) {
		gtk_text_buffer_insert(textbuff, &textiter, "\n", -1);
	}
	gtk_text_buffer_insert(textbuff, &textiter, text, length);

	/* Scroll to the end mark */
	textmark = gtk_text_buffer_get_mark(textbuff, "endmark");
	gtk_text_view_scroll_to_mark(GTK_TEXT_VIEW(currentgame->Chat), textmark,
			0.0, FALSE, 0.0, 0.0);
}


game *game_new(gint32 id) {
	game *game;

	game = g_malloc0(sizeof(*game));
	game->gameid = id;
	printf("New game: %d\n", game->gameid);

	game->next = game_h;
	game_h = game;
	return game;
}


void game_free(gint32 id) {
	game **prevp, *g;

	for (prevp = &game_h; (g = *prevp); prevp = &g->next) {
		if (g->gameid == id) {
			*prevp = g->next;
			printf("Deleted game: %d\n", g->gameid);
			g_free(g);
			break;
		}
	}
}


game *game_find(gint32 id) {
	game *g;

	for (g = game_h; g; g = g->next) {
		if (g->gameid == id) {
			return g;
		}
	}
	return NULL;
}


/* free all game ressources, return to get games page */
void game_quit()  {
	gint32 i;
	game *g;

	client_disconnect(global->game_connect);

	currentgame->ChatBox = NULL;
	currentgame->Chat = NULL;

	if(currentgame->WinEstateTree)  gtk_widget_destroy(currentgame->WinEstateTree);

	if(currentgame->timeout_token)  g_source_remove(currentgame->timeout_token);

	game_delete_cookie();

	/* remove all players */
	while (player_h) {
		game_free_player(player_h);
	}

	for(i = 0 ; i < data->number_estates ; i++)  {

		if(currentgame->estate[i].name) g_free(currentgame->estate[i].name);
	}

	for(i = 0 ; i < MAX_GROUPS ; i++)  {

		if(currentgame->group[i].name) g_free(currentgame->group[i].name);
	}

	for(i = 0 ; i < MAX_TRADES ; i++)  {

		trade_destroy_slot(i);
	}

	for(i = 0 ; i < MAX_COMMANDS ; i++)  {

		if(currentgame->command[i].open  &&  currentgame->command[i].command)
			g_free(currentgame->command[i].command);
	}

	for(i = 0 ; i < MAX_CARDS ; i++)  {

		if (currentgame->card[i].title) {
			g_free(currentgame->card[i].title);
		}
	}

	eng_frame_destroy( currentgame->board_frame );

	game_free_pngs();

	/* remove all games */
	g = game_h;
	while (g) {
		game *cur = g;
		g = g->next;
		printf("Deleted game: %d\n", cur->gameid);
		g_free(cur);
	}
	game_h = NULL;

	currentgame = NULL;

	interface_create_getgamespage(true);
}


/* create a new player slot */
player *game_new_player(guint32 id) {
	player *player;

	player = g_malloc0(sizeof(*player));
	player->playerid = id;
	player->tokenid = -1;
	printf("New player: %d\n", player->playerid);

	player->next = player_h;
	player_h = player;
	return player;
}


player *player_from_id(gint32 playerid) {
	player *p;

	for (p = player_h; p; p = p->next) {
		if ((gint32)p->playerid == playerid) {
			return p;
		}
	}
	return NULL;
}


player *player_from_name(gchar *name)  {
	player *p;

	if (!name) return NULL;
	if (strlen(name) <= 0) return NULL;

	for (p = player_h; p; p = p->next) {
		if (!p->name) continue;

		if (!strcmp(p->name, name)) {
			return p;
		}
	}
	return NULL;
}


gint32 get_playerlistcard_id_with_estate(gint32 estate)  {

	gint32 i;

	for(i = 0 ; i < data->number_playerlist_card ; i++)  {

		if(data->playerlist_card[i].estateid == estate) {

			return(i);
			break;
		}
	}

	return(-1);
}


/* return a valid command slotid */
gboolean game_get_valid_command_slot(gint32 *commandslot)  {

	gint32 i;

	for(i = 0; i < MAX_COMMANDS ; i++)
		if(!currentgame->command[i].open) {

			*commandslot = i;
			return TRUE;
		}

	return FALSE;
}


/* return commandslot if button command is displayed
   return <0 if button not is not displayed           */
gint32 get_command_button_slot_with_command(gchar *command) {

	gint32 i;

	for(i = 0 ; i < MAX_COMMANDS ; i ++)  {

		if(currentgame->command[i].open)  {

			if(! strncmp(command, currentgame->command[i].command, strlen(command)) )
				return i;
		}
	}

	return -1;
}


/* send specific chat message, like version */
void parse_specific_chat_message(gchar *message)   {

	gchar *msg, *sendstr;
	player *my_player;

	struct timeval tv;
	time_t t_now;

	my_player = player_from_id(global->my_playerid);

	if(message[0] != '!') return;

	msg = message;
	msg++;

	if(! strncmp(msg, "version", 7))  {

		msg += 7;
		while( strlen(msg) )  {

			if(msg[0] != ' ') break;
			else  msg++;
		}

		if (strlen(msg) == 0 || !strcmp(msg, my_player->name)) {

			sendstr = g_strdup_printf("GtkAtlantic %s\n", VERSION);
			client_send(global->game_connect, sendstr);
			g_free(sendstr);
		}
	}


	else if(! strncmp(msg, "date", 4))  {

		msg += 4;
		while( strlen(msg) )  {

			if(msg[0] != ' ') break;
			else  msg++;
		}

		if (strlen(msg) == 0 || !strcmp(msg, my_player->name)) {

			gettimeofday(&tv, NULL);
			t_now = tv.tv_sec;
			sendstr = g_strdup_printf("%s", ctime(&t_now) );
			client_send(global->game_connect, sendstr);
			g_free(sendstr);
		}
	}

	else if(! strncmp(msg, "ping", 4))  {

		msg += 4;
		while( strlen(msg) )  {

			if(msg[0] != ' ') break;
			else  msg++;
		}

		if (strlen(msg) == 0 || !strcmp(msg, my_player->name)) {

			sendstr = g_strdup_printf("pong\n");
			client_send(global->game_connect, sendstr);
			g_free(sendstr);
		}
	}
}


/* parse specific send message */
void parse_specific_send_message(gchar *message)   {

	gchar *msg, *tmp;
	gint32 i;

	struct timeval tv;
	struct tm *local_tm;
	time_t t_now;

	if(message[0] != '/') return;

	msg = message;
	msg++;

	if(! strncmp(msg, "assets", 6))  {

		msg += 6;
		while( strlen(msg) )  {

			if(msg[0] != ' ') break;
			else  msg++;
		}

		if(strlen(msg) == 0)  {
			player *p;
			for (p = player_h; p; p = p->next) {
				if (p->spectator) continue;
				if (p->game != currentgame->gameid) continue;

				tmp = g_strdup_printf("<%s> %d", p->name, game_get_assets_player(p->playerid) );
				text_insert_chat(tmp, strlen(tmp) );
				g_free(tmp);
			}
		}

		player *p;
		if ((p = player_from_name(msg))) {
			tmp = g_strdup_printf("<%s> %d", p->name, game_get_assets_player(p->playerid) );
			text_insert_chat(tmp, strlen(tmp));
			g_free(tmp);
		}
	}


	else if(! strncmp(msg, "nick", 4))  {

		msg += 4;
		while( strlen(msg) )  {

			if(msg[0] != ' ') break;
			else  msg++;
		}

		if (strlen(msg) != 0 && strcmp(player_from_id(global->my_playerid)->name, msg)) {

			tmp = g_strdup_printf(".n%s\n", msg);
			client_send(global->game_connect, tmp);
			g_free(tmp);
		}
	}


	else if(! strncmp(msg, "date", 4))  {
		gint32 len;

		gettimeofday(&tv, NULL);
		t_now = tv.tv_sec;

		tmp = g_strdup_printf("%s", ctime(&t_now) );
		len = strlen(tmp);

		for(i = 0 ; i < len ; i++)  {

			if(tmp[i] == '\n')  {
				tmp[i] = '\0';
				break;
			}
		}
		text_insert_chat(tmp, strlen(tmp) );
		g_free(tmp);
	}


	else if(! strncmp(msg, "elapsed", 7))  {

		if(currentgame->status >= GAME_STATUS_RUN)  {

			gettimeofday(&tv, NULL);
			t_now = tv.tv_sec - currentgame->start_time;
		}
		else  t_now = 0;

		local_tm = gmtime(&t_now);

		tmp = g_strdup_printf("%.2d:%.2d:%.2d", local_tm->tm_hour, local_tm->tm_min, local_tm->tm_sec);
		text_insert_chat(tmp, strlen(tmp) );
		g_free(tmp);
	}

	else if(! strncmp(msg, "me", 2))  {

		msg += 2;
		while( strlen(msg) )  {

			if(msg[0] != ' ') break;
			else  msg++;
		}

		if(strlen(msg) != 0)  {

			tmp = g_strdup_printf("[ACTION] %s\n", msg);
			client_send(global->game_connect, tmp);
			g_free(tmp);
		}
	}
}


/* sort playerlist by playerid (ascending) */
void game_sort_playerlist_by_playerid(void)  {
	player *sorted_h = NULL;
	while (player_h) {
		player **pp, *p, **spp, *sp;
		for (pp = &player_h, sp = *pp, spp = pp; (p = *pp); pp = &p->next) {
			if (sp->playerid < p->playerid) {
				sp = p;
				spp = pp;
			}
		}
		*spp = sp->next; /* remove from list */
		sp->next = sorted_h;
		sorted_h = sp;
	}
	player_h = sorted_h;
}


/* sort playerlist by turnorder (ascending) */
void game_sort_playerlist_by_turnorder(void)  {
	player *sorted_h = NULL;
	while (player_h) {
		player **pp, *p, **spp, *sp;
		for (pp = &player_h, sp = *pp, spp = pp; (p = *pp); pp = &p->next) {
			if (sp->turnorder < p->turnorder) {
				sp = p;
				spp = pp;
			}
		}
		*spp = sp->next; /* remove from list */
		sp->next = sorted_h;
		sorted_h = sp;
	}
	player_h = sorted_h;
}


void game_switch_status() {
	guint32 i;

	client_disconnect(global->customserver_connect);
	client_disconnect(global->metaserver_connect);

	switch (currentgame->status) {
		case GAME_STATUS_CONFIG:
			interface_create_gameconfigpage();
			break;

		case GAME_STATUS_INIT:
			interface_create_gameinitpage();
			if (game_load_pngs()) exit(-1);
			break;

		case GAME_STATUS_RUN:
			if (game_load_pngs()) exit(-1);
			interface_create_gameboardpage();
			game_write_cookie();
			break;

		case GAME_STATUS_END:
			game_delete_cookie();
			if (currentgame->timeout_token) {
				g_source_remove(currentgame->timeout_token);
				currentgame->timeout_token = 0;
			}
			/* destroy all open trades */
			for(i = 0 ; i < MAX_TRADES ; i++) {
				trade_destroy_slot(i);
			}
			break;
			
		default:
			return;
	}
}


/* write cookie on disk */
void game_write_cookie()  {

	gchar *filename, *errormsg;
	FILE *file;

	if (!global->path_home) {
		return;
	}

	filename = g_strconcat(global->path_home, "/cookie", NULL);

	file = fopen(filename, "wt");
	if (!file) {
		errormsg = g_strdup_printf("Can't write cookie file at \"%s\"", filename);
		interface_set_infolabel(errormsg, "b00000", false);
		g_free(errormsg);
		g_free(filename);
		return;
	}

	fprintf(file, "%s\n%d\n%s\n",
		global->game_connect->host,
		global->game_connect->port,
		global->cookie);

	g_free(filename);
	fclose(file);
}


void game_delete_cookie(void) {
	if (!global->path_home) {
		return;
	}

	gchar *filename = g_strconcat(global->path_home, "/cookie", NULL);
	unlink(filename);
	g_free(filename);
}


gboolean game_cookie_available(void) {
	struct stat st;
	int r;

	if (!global->path_home) {
		return false;
	}

	gchar *filename = g_strconcat(global->path_home, "/cookie", NULL);
	r = stat(filename, &st);
	g_free(filename);

	return (r == 0);
}


/* return a valid card slotid */
gboolean game_get_valid_card_slot(gint32 *cardslot)  {

	gint32 i;

	for(i = 0; i < MAX_CARDS ; i++)
		if(!currentgame->card[i].owner) {

			*cardslot = i;
			return TRUE;
		}

	return FALSE;
}


/* return the card slot by cardid */
gint32 get_card_slot_with_cardid(gint32 cardid)  {

	gint32 i;

	for(i = 0 ; i < MAX_CARDS ; i++)  {

		if(currentgame->card[i].cardid == cardid) {

			return(i);
			break;
		}
	}

	return(-1);
}


gint32 game_nb_players() {
	gint32 c = 0;
	player *p;

	for (p = player_h; p; p = p->next) {
		if (p->spectator) continue;
		if (p->game != currentgame->gameid) continue;
		c++;
	}
	return c;
}

/* return a valid trade slotid */
gboolean game_get_valid_trade_slot(gint32 *tradeslot)  {

	gint32 i;

	for(i = 0; i < MAX_TRADES ; i++)
		if(! currentgame->trade[i].open) {

			*tradeslot = i;
			return TRUE;
		}

	return FALSE;
}


/* return trade slot by tradeid */
gint32 get_trade_slot_with_tradeid(gint32 tradeid)  {

	gint32 i;

	for(i = 0 ; i < MAX_TRADES ; i++)  {

		if(! currentgame->trade[i].open) continue;

		if(currentgame->trade[i].tradeid == tradeid) {

			return(i);
			break;
		}
	}

	return(-1);
}



/* return estate identifier by name of this estate */
gint32 get_estateid_by_estatename(gchar *name)  {

	gint32 i;

	for(i = 0 ; i < data->number_estates ; i++)  {

		if(! strcmp(currentgame->estate[i].name, name) )  {

			return(i);
			break;
		}
	}

	return(-1);
}


/* build player list in game config phase */
void game_buildplayerlist()   {

	if (!currentgame) {
		return;
	}

	if (global->my_playerid == currentgame->master) {
		GtkWidget *MenuItem = g_object_get_data(G_OBJECT(global->Menu), "upgrade_spectator");
		gtk_widget_destroy(gtk_menu_item_get_submenu(GTK_MENU_ITEM(MenuItem)));
		GtkWidget *SubMenu = gtk_menu_new();
		gtk_menu_item_set_submenu(GTK_MENU_ITEM(MenuItem), SubMenu);

		gtk_widget_set_sensitive(MenuItem, FALSE);
		player *p;
		for (p = player_h; p; p = p->next) {
			if (!p->spectator) continue;
			if (p->game != currentgame->gameid) continue;

			GtkWidget *item = gtk_menu_item_new_with_label(p->name);
			g_object_set_data(G_OBJECT(item), "player", p);
			g_signal_connect(G_OBJECT(item), "activate", G_CALLBACK(CallBack_menu_AddPlayer), NULL);
			gtk_menu_shell_append(GTK_MENU_SHELL(SubMenu), item);
			gtk_widget_set_sensitive(MenuItem, TRUE);
		}
		gtk_widget_show_all(SubMenu);
	}

	/* build player list */
	if (global->phase == PHASE_GAMECREATE) {
		GtkListStore *store = GTK_LIST_STORE(gtk_tree_view_get_model(GTK_TREE_VIEW(currentgame->PlayerList)));
		GtkTreeIter iter;
		player *p;

		gtk_list_store_clear(store);
		game_sort_playerlist_by_playerid();

		for (p = player_h; p; p = p->next) {
			if (p->spectator) continue;
			if (p->game != currentgame->gameid)  continue;

			gtk_list_store_append(store, &iter);
			gtk_list_store_set(store, &iter,
				PLAYERLIST_COLUMN_NAME, p->name,
				PLAYERLIST_COLUMN_HOST, p->host,
				-1);
		}
	}
}


/* return assets of a player,
 *   assets is: money + sale price of houses + unmortgaged value of estates
 */
gint32 game_get_assets_player(gint32 playerid)  {
	gint32 i, assets;

	player *p = player_from_id(playerid);
	if (p == NULL)  return 0;

	/* money */
	assets = p->money;

	/* estates + houses */
	for (i = 0; i < data->number_estates; i++)  {

		if (currentgame->estate[i].owner != playerid) continue;

		/* estates */
		if (!currentgame->estate[i].mortgaged)
			assets += currentgame->estate[i].mortgageprice;

		/* houses */
		if (currentgame->estate[i].houses <= 0) continue;

		assets += currentgame->estate[i].houses * currentgame->estate[i].sellhouseprice;
	}

	return assets;
}


/* update token position, handle superposed token */
void game_update_tokens()  {

	gint32 i, nb_tokens, x1, y1, x2, y2, xs, ys, xp, yp, k;
	player *p, *pp;


	/* === UNJAILED === */

	for (i = 0; i < data->number_estates; i++) {

		/* search number of unjailed tokens on this estate */
		for (nb_tokens = 0, p = player_h; p; p = p->next) {
			if (p->spectator) continue;
			if (p->game != currentgame->gameid) continue;
			if (p->bankrupt) continue;
			if (p->jailed) continue;
			if (p->location != i) continue;

			nb_tokens++;
		}

		/* move token */
		if (nb_tokens == 0) continue;

		for (k = 0, xp = -1, yp = -1, pp = NULL, p = player_h; p; p = p->next) {
			if (p->spectator) continue;
			if (p->game != currentgame->gameid) continue;
			if (p->bankrupt) continue;
			if (p->jailed) continue;
			if (p->location != i) continue;

			x1 = data->estate[p->location].x1token;
			x2 = data->estate[p->location].x2token;
			y1 = data->estate[p->location].y1token;
			y2 = data->estate[p->location].y2token;

			if(nb_tokens == 1)  {

				eng_pic_set_x(p->token_pic, x1);
				eng_pic_set_y(p->token_pic, y1);

				break;
			}

			xs = ( ( (x2 - x1) * k) / (nb_tokens -1) ) + x1;
			if(pp  &&  abs(xs - xp) > (data->pngfile_token_width[pp->tokenid] + 2) ) {

				if(xs - xp > 0)
					xs = xp + data->pngfile_token_width[pp->tokenid] + 2;
				else
					xs = xp - data->pngfile_token_width[pp->tokenid] - 2;
			}

			ys = ( ( (y2 - y1) * k) / (nb_tokens -1) ) + y1;
			if(pp  &&  abs(ys - yp) > (data->pngfile_token_height[pp->tokenid] + 2) ) {

				if(ys - yp > 0)
					ys = yp + data->pngfile_token_height[pp->tokenid] + 2;
				else
					ys = yp - data->pngfile_token_height[pp->tokenid] - 2;
			}

			eng_pic_set_x(p->token_pic, xs);
			eng_pic_set_y(p->token_pic, ys);

			xp = xs;
			yp = ys;
			pp = p;

			k++;
		}
	}


	/* === JAILED === */

	for (i = 0; i < data->number_estates; i++) {

		/* search number of jailed tokens on this estate */
		for (nb_tokens = 0, p = player_h; p; p = p->next) {
			if (p->spectator) continue;
			if (p->game != currentgame->gameid) continue;
			if (p->bankrupt) continue;
			if (!p->jailed) continue;
			if (p->location != i) continue;

			nb_tokens++;
		}

		/* move token */
		if (nb_tokens == 0) continue;

		for (k = 0, xp = -1, yp = -1, pp = NULL, p = player_h; p; p = p->next) {
			if (p->spectator) continue;
			if (p->game != currentgame->gameid) continue;
			if (p->bankrupt) continue;
			if (!p->jailed) continue;
			if (p->location != i) continue;

			x1 = data->estate[p->location].x1jail;
			x2 = data->estate[p->location].x2jail;
			y1 = data->estate[p->location].y1jail;
			y2 = data->estate[p->location].y2jail;

			if(nb_tokens == 1)  {

				eng_pic_set_x(p->token_pic, x1);
				eng_pic_set_y(p->token_pic, y1);

				break;
			}

			xs = ( ( (x2 - x1) * k) / (nb_tokens -1) ) + x1;
			if(pp  &&  abs(xs - xp) > (data->pngfile_token_width[pp->tokenid] + 2) ) {

				if(xs - xp > 0)
					xs = xp + data->pngfile_token_width[pp->tokenid] + 2;
				else
					xs = xp - data->pngfile_token_width[pp->tokenid] - 2;
			}

			ys = ( ( (y2 - y1) * k) / (nb_tokens -1) ) + y1;
			if(pp  &&  abs(ys - yp) > (data->pngfile_token_height[pp->tokenid] + 2) ) {

				if(ys - yp > 0)
					ys = yp + data->pngfile_token_height[pp->tokenid] + 2;
				else
					ys = yp - data->pngfile_token_height[pp->tokenid] - 2;
			}

			eng_pic_set_x(p->token_pic, xs);
			eng_pic_set_y(p->token_pic, ys);

			xp = xs;
			yp = ys;
			pp = p;

			k++;
		}
	}

	update_display();
}


/* initiate token movement */
void game_initiate_token_movement() {

	if (global->phase != PHASE_GAMEPLAY) {
		return;
	}
	if (currentgame->status != GAME_STATUS_RUN) {
		return;
	}

	/* do nothing if token timer is running */
	if (currentgame->timeout_token) {
		return;
	}

	game_move_tokens(NULL);
}


/* animate token movement */
gboolean game_move_tokens(gpointer ptr)  {
	gchar *sendstr;
	gboolean update = FALSE, more = FALSE;
	player *p;
	(void)ptr;

	for (p = player_h; p; p = p->next) {
		if (p->spectator) continue;
		if (p->game != currentgame->gameid) continue;

		if (p->directmove) {
			p->location = p->location_to;
			p->directmove = 0;
		} else if (p->location != p->location_to) {
			p->location++;
			if (p->location >= data->number_estates) {
				p->location = 0;
			}
			if (p->location != p->location_to) {
				more = TRUE;
			}
		} else {
			continue;
		}

		sendstr = g_strdup_printf(".t%d\n", p->location);
		client_send(global->game_connect, sendstr);
		g_free(sendstr);

		update = TRUE;
	}

	if (update) {
		game_update_tokens();

		if (more) {
			if (currentgame->timeout_token == 0) {
				currentgame->timeout_token = g_timeout_add(config->game_token_animation_speed, game_move_tokens, NULL);
			}
			return TRUE;
		}
	}

	currentgame->timeout_token = 0;
	return FALSE;
}


void game_free_player(player *p)  {
	player **prevp, *pl;

	for (prevp = &player_h; (pl = *prevp); prevp = &pl->next) {
		if (pl != p) {
			continue;
		}

		*prevp = p->next;

		/* delete a player */
		eng_frame_destroy(p->playerlist_token_frame);
		eng_frame_destroy(p->playerlist_cards_frame);

		if (p->name) g_free(p->name);
		if (p->host) g_free(p->host);
		if (p->image) g_free(p->image);

		printf("Deleted player: %d\n", p->playerid);
		g_free(p);
		return;
	}
}

void game_exit(void) {

	if (global->phase >= PHASE_GAMECREATE)  {
		client_send(global->game_connect, ".gx\n");
	}

	game_delete_cookie();
	game_free_pngs();
}

/*
 *     gtkatlantic - the gtk+ monopd client, enjoy network monopoly games
 *
 *
 *  Copyright © 2002-2015 Sylvain Rochet
 *
 *  gtkatlantic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; see the file COPYING. If not, see
 *  <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <gtk/gtk.h>

#include "global.h"

#include "display.h"
#include "callback.h"


void display_init()  {

	GtkWidget *vbox;
	GtkWidget *text;
	GtkTextBuffer *textbuff;
	GtkTextIter textiter;
	GtkWidget *scrolledwin;

	vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 15);
	gtk_container_set_border_width(GTK_CONTAINER(vbox), 15);
	g_object_set_data(G_OBJECT(currentgame->BoardCenter), "boardcenter", vbox);
	gtk_box_pack_start(GTK_BOX(currentgame->BoardCenter), vbox, TRUE, TRUE, 0);

	/* create text view */
	scrolledwin = gtk_scrolled_window_new(NULL,NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolledwin), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start(GTK_BOX(vbox), scrolledwin, TRUE, TRUE, 0);

	text = gtk_text_view_new();
	gtk_text_view_set_editable(GTK_TEXT_VIEW(text), FALSE);
	gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(text), GTK_WRAP_WORD);
	textbuff = gtk_text_view_get_buffer(GTK_TEXT_VIEW(text));
	gtk_text_buffer_get_end_iter(textbuff, &textiter);
	gtk_text_buffer_create_mark(textbuff, "endmark", &textiter, FALSE);
	g_object_set_data(G_OBJECT(currentgame->BoardCenter), "boardcenter_text", text);
	gtk_container_add(GTK_CONTAINER(scrolledwin), text);

	gtk_widget_show_all(vbox);
}


void display_hide()  {
	GtkWidget *vbox= g_object_get_data(G_OBJECT(currentgame->BoardCenter), "boardcenter");
	gtk_widget_hide(vbox);
}


void display_show()  {
	GtkWidget *vbox = g_object_get_data(G_OBJECT(currentgame->BoardCenter), "boardcenter");
	gtk_widget_show(vbox);
}


void display_clear_text() {
	GtkWidget *text;
	GtkTextBuffer *textbuff;
	GtkTextIter startiter, enditer;

	text = g_object_get_data(G_OBJECT(currentgame->BoardCenter), "boardcenter_text");
	textbuff = gtk_text_view_get_buffer(GTK_TEXT_VIEW(text));
	gtk_text_buffer_get_bounds(textbuff, &startiter, &enditer);
	gtk_text_buffer_delete(textbuff, &startiter, &enditer);
}


void display_clear_buttons() {
	GtkWidget *buttonsbox = g_object_get_data(G_OBJECT(currentgame->BoardCenter), "boardcenter_buttons");
	if (buttonsbox) {
		gtk_widget_destroy(buttonsbox);
		g_object_set_data(G_OBJECT(currentgame->BoardCenter), "boardcenter_buttons", NULL);
	}
}


void display_estate(gint32 estateid)  {
	display_title(estateid < 0 ? NULL : currentgame->estate[estateid].name);
}


void display_title(gchar *title) {

	GtkWidget *label = g_object_get_data(G_OBJECT(currentgame->BoardCenter), "boardcenter_title");

	if (!title) {
		if (label) {
			gtk_widget_destroy(label);
			g_object_set_data(G_OBJECT(currentgame->BoardCenter), "boardcenter_title", NULL);
		}
		return;
	}

	if (!label) {
		GtkWidget *vbox = g_object_get_data(G_OBJECT(currentgame->BoardCenter), "boardcenter");
		label = gtk_label_new(NULL);
		gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
		gtk_label_set_line_wrap(GTK_LABEL(label), TRUE);
		gtk_widget_set_halign(label, GTK_ALIGN_CENTER);
		gtk_widget_set_valign(label, GTK_ALIGN_CENTER);
		gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 0);
		gtk_box_reorder_child(GTK_BOX(vbox), label, 0);
		g_object_set_data(G_OBJECT(currentgame->BoardCenter), "boardcenter_title", label);
		gtk_widget_show(label);
	}

	gchar *tmp = g_markup_printf_escaped("<span size=\"xx-large\"><b>%s</b></span>", title);
	gtk_label_set_markup(GTK_LABEL(label), tmp);
	g_free(tmp);
}


void display_text(gchar *message) {

	GtkWidget *text;
	GtkTextBuffer *textbuff;
	GtkTextIter textiter;
	GtkTextMark *textmark;

	text = g_object_get_data(G_OBJECT(currentgame->BoardCenter), "boardcenter_text");
	textbuff = gtk_text_view_get_buffer(GTK_TEXT_VIEW(text));
	gtk_text_buffer_get_end_iter(textbuff, &textiter);
	if (gtk_text_iter_get_offset(&textiter)) {
		gtk_text_buffer_insert(textbuff, &textiter, "\n", 1);
	}
	gtk_text_buffer_insert(textbuff, &textiter, message, strlen(message));

	/* Scroll to the end mark */
	textmark = gtk_text_buffer_get_mark(textbuff, "endmark");
	gtk_text_view_scroll_to_mark(GTK_TEXT_VIEW(text), textmark,
			0.0, FALSE, 0.0, 0.0);
}


void display_add_button(gchar *caption, gchar *command, gboolean enabled) {

	GtkWidget *buttonsbox;
	GtkWidget *button;
	gchar *text;

	buttonsbox = g_object_get_data(G_OBJECT(currentgame->BoardCenter), "boardcenter_buttons");
	if (!buttonsbox) {
		GtkWidget *vbox = g_object_get_data(G_OBJECT(currentgame->BoardCenter), "boardcenter");
		buttonsbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 15);
		gtk_box_pack_start(GTK_BOX(vbox), buttonsbox, FALSE, FALSE, 0);
		g_object_set_data(G_OBJECT(currentgame->BoardCenter), "boardcenter_buttons", buttonsbox);
		gtk_widget_show(buttonsbox);
	}

	button = gtk_button_new_with_label(caption);
	gtk_box_pack_start(GTK_BOX(buttonsbox), button, TRUE, TRUE, 0);

	text = g_strdup_printf("%s\n", command );
	g_object_set_data_full(G_OBJECT(button), "command", text, g_free);
	g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(CallBack_button_command_pressed), NULL);
	gtk_widget_set_sensitive(button, enabled);

	gtk_widget_show(button);
}

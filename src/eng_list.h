/*
 *     gtkatlantic - the gtk+ monopd client, enjoy network monopoly games
 *
 *
 *  Copyright © 2002-2015 Sylvain Rochet
 *
 *  gtkatlantic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; see the file COPYING. If not, see
 *  <http://www.gnu.org/licenses/>.
 */

#ifndef ENG_LIST_H
#define ENG_LIST_H

#include <glib.h>

typedef struct eng_list_ eng_list;
typedef struct eng_list_e_ eng_list_e;


struct eng_list_  {

	eng_list_e *first, *last;
	guint32 length;
};


struct eng_list_e_  {

	eng_list *list;
	eng_list_e *prev;
	eng_list_e *next;
	void *data;
};

eng_list *eng_list_new();
eng_list *eng_list_destroy(eng_list *l);
eng_list_e *eng_list_element_new(eng_list *l, void *d);
eng_list_e *eng_list_append(eng_list *l, void *d);
eng_list_e *eng_list_prepend(eng_list *l, void *d);
void eng_list_remove_fast(eng_list_e *le);
void eng_list_remove(eng_list *l, void *d);
eng_list_e *eng_list_first(eng_list *l);
eng_list_e *eng_list_last(eng_list *l);
eng_list_e *eng_list_previous(eng_list_e *le);
eng_list_e *eng_list_next(eng_list_e *le);
void eng_list_clean(eng_list *l);
guint32 eng_list_length(eng_list *l);
eng_list_e *eng_list_find(eng_list *l, void *d);
GList *eng_list_libevlist_to_glist(eng_list *l);
eng_list *eng_list_glist_to_libevlist(GList *gl);

#endif /* ENG_LIST_H */

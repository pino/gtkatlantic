/*
 *     gtkatlantic - the gtk+ monopd client, enjoy network monopoly games
 *
 *
 *  Copyright © 2002-2015 Sylvain Rochet
 *
 *  gtkatlantic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; see the file COPYING. If not, see
 *  <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>

#include <gtk/gtk.h>
#include <libxml/parser.h>

#include "engine.h"

#include "xmlparse.h"
#include "game.h"
#include "client.h"
#include "interface.h"
#include "global.h"
#include "load.h"
#include "callback.h"
#include "display.h"
#include "trade.h"

/*
ok	server
ok	client
ok	msg
ok	display
dep	updateplayerlist
dep	updategamelist
ok	playerupdate
ok	deleteplayer
ok	estateupdate
ok	cardupdate
ok	estategroupupdate
ok	tradeupdate
ok	auctionupdate
ok	gameupdate
ok	deletegame
dep	commandlist
ok	configupdate
*/

/*
ok	.R

ok	.gn
no-need	.gl
ok	.gj
ok	.gx
todo	.gk
todo	.gu
ok	.gc
todo	.gS

ok	.Tn
ok	.Tc
ok	.Te
ok	.Tm
ok	.Ta
ok	.Tr

ok	.r
ok	.E
ok	.t

ok	.D
no-need	.p

ok	.eb
ok	.es
ok	.ea
ok	.hb
ok	.hs
ok	.em

ok	.ab

ok	.jc
ok	.jp
ok	.jr

ok	.T$
ok	.T%

no-need	.pi

todo	.gd
ok	.gs

no-need	.d
ok	.n
no-need	.f
*/


void xmlparse_getgamelist_plugger(connection *c, gchar *buffer)  {

	xmlDocPtr doc;
	xmlNodePtr cur;

	doc = xmlParseMemory(buffer, strlen(buffer));
	if (doc == NULL) {
		return;
	}

	cur = xmlDocGetRootElement(doc);
	if (cur == NULL) {
		xmlFreeDoc(doc);
		return;
	}

	if( xmlStrcmp(cur->name, SERVER_XMLROOTELEMENT) )  {
		xmlFreeDoc(doc);
		return;
	}

	for( cur = cur->xmlChildrenNode ; cur != NULL ; cur = cur -> next)  {

		if(! xmlStrcmp(cur->name, (xmlChar*)"server") )  xmlparse_server(c, doc, cur);
		if(! xmlStrcmp(cur->name, (xmlChar*)"gameupdate") )  xmlparse_gamelist_gameupdate(c, doc, cur);
		if(! xmlStrcmp(cur->name, (xmlChar*)"deletegame") )  xmlparse_gamelist_deletegame(c, doc, cur);
	}

	xmlFreeDoc(doc);
}




void xmlparse_game_plugger(connection *c, gchar *buffer)  {

	xmlDocPtr doc;
	xmlNodePtr cur;

	doc = xmlParseMemory(buffer, strlen(buffer));
	if (doc == NULL) {
		return;
	}

	cur = xmlDocGetRootElement(doc);
	if (cur == NULL) {
		xmlFreeDoc(doc);
		return;
	}

	if( xmlStrcmp(cur->name, SERVER_XMLROOTELEMENT) )  {
		xmlFreeDoc(doc);
		return;
	}

	for(cur = cur->xmlChildrenNode ; cur != NULL ; cur = cur -> next)  {

		if(! xmlStrcmp(cur->name, (xmlChar*)"gameupdate") )  xmlparse_gameupdate(c, doc, cur);
		if(! xmlStrcmp(cur->name, (xmlChar*)"deletegame") )  xmlparse_deletegame(c, doc, cur);
		if(! xmlStrcmp(cur->name, (xmlChar*)"server") )  xmlparse_server(c, doc, cur);
		if(! xmlStrcmp(cur->name, (xmlChar*)"client") )  xmlparse_client(c, doc, cur);
		if(! xmlStrcmp(cur->name, (xmlChar*)"msg") )  xmlparse_message(c, doc, cur);
		if(! xmlStrcmp(cur->name, (xmlChar*)"estateupdate") )  xmlparse_estateupdate(c, doc, cur);
		if(! xmlStrcmp(cur->name, (xmlChar*)"playerupdate") )  xmlparse_playerupdate(c, doc, cur);
		if(! xmlStrcmp(cur->name, (xmlChar*)"auctionupdate") )  xmlparse_auctionupdate(c, doc, cur);
		if(! xmlStrcmp(cur->name, (xmlChar*)"display") )  xmlparse_display(c, doc, cur);
		if(! xmlStrcmp(cur->name, (xmlChar*)"configupdate") )  xmlparse_configupdate(c, doc, cur);
		if(! xmlStrcmp(cur->name, (xmlChar*)"tradeupdate") )  xmlparse_tradeupdate(c, doc, cur);
		if(! xmlStrcmp(cur->name, (xmlChar*)"cardupdate") )  xmlparse_cardupdate(c, doc, cur);
		if(! xmlStrcmp(cur->name, (xmlChar*)"estategroupupdate") )  xmlparse_estategroupupdate(c, doc, cur);
		if(! xmlStrcmp(cur->name, (xmlChar*)"deleteplayer") )  xmlparse_deleteplayer(c, doc, cur);
	}

	xmlFreeDoc(doc);
}




void xmlparse_server(connection *c, xmlDocPtr doc, xmlNodePtr cur)  {
	(void)doc;

	c->server_version = (gchar*)xmlGetProp(cur, (xmlChar*)"version");
}




void xmlparse_client(connection *c, xmlDocPtr doc, xmlNodePtr cur)  {

	xmlChar *tmp;
	(void)c;
	(void)doc;

	tmp = xmlGetProp(cur, (xmlChar*)"playerid");
	if( tmp )  {
		global->my_playerid = atoi( (gchar*)tmp );
		g_free(tmp);
	}

	tmp = xmlGetProp(cur, (xmlChar*)"cookie");
	if( tmp )  {
		if(global->cookie) g_free(global->cookie);
		global->cookie = (gchar*)tmp;
	}
}




void xmlparse_gameupdate(connection *c, xmlDocPtr doc, xmlNodePtr cur) {
	xmlChar *tmp;
	struct timeval tv;
	gint32 gameid;
	game *game;
	guint8 newstatus;
	(void)c;
	(void)doc;

	tmp = xmlGetProp(cur, (xmlChar*)"gameid");
	if (tmp) {
		gameid = atoi((gchar*)tmp);
		g_free(tmp);
	} else {
		return;
	}

	/* We do not need game templates */
	if (gameid < 0) {
		return;
	}

	game = game_find(gameid);
	if (!game) {
		game = game_new(gameid);
		if (!game) {
			return;
		}
	}

	tmp = xmlGetProp(cur, (xmlChar*)"status");
	if(tmp) {
		if(!xmlStrcmp(tmp, (xmlChar*)"config") ) {
			newstatus = GAME_STATUS_CONFIG;
		}
		else if(!xmlStrcmp(tmp, (xmlChar*)"init") ) {
			newstatus = GAME_STATUS_INIT;
		}
		else if(!xmlStrcmp(tmp, (xmlChar*)"run") ) {
			newstatus = GAME_STATUS_RUN;
		}
		else if(!xmlStrcmp(tmp, (xmlChar*)"end") ) {
			newstatus = GAME_STATUS_END;
		}
		else {
			g_free(tmp);
			return;
		}

		if (game->status != newstatus) {
			game->status = newstatus;

			if (newstatus == GAME_STATUS_RUN) {
				gettimeofday(&tv, NULL);
				game->start_time = tv.tv_sec;
			}

			if (currentgame == game) {
				game_switch_status();
			}
		}
	}
	g_free(tmp);

	tmp = xmlGetProp(cur, (xmlChar*)"master");
	if (tmp)  {
		game->master = atoi((gchar*)tmp);
		g_free(tmp);

		if (game->GameConfigBox) {
			GList *list = gtk_container_get_children(GTK_CONTAINER(game->GameConfigBox));
			for (list = g_list_first(list); list; list = g_list_next(list)) {
				gtk_widget_set_sensitive(GTK_WIDGET(list->data), (global->my_playerid == game->master));
			}
			g_list_free(list);
		}

		/* set sensitive mode of start button */
		if (currentgame == game && game->status == GAME_STATUS_CONFIG) {
			GtkWidget *StartButton = g_object_get_data(G_OBJECT(global->MainVerticalBox), "start_button");
			gtk_widget_set_sensitive(StartButton, (global->my_playerid == game->master));
		}
	}
}


void xmlparse_deletegame(connection *c, xmlDocPtr doc, xmlNodePtr cur) {
	xmlChar *tmp;
	gint32 gameid;
	(void)c;
	(void)doc;

	tmp = xmlGetProp(cur, (xmlChar*)"gameid");
	if (tmp) {
		gameid = atoi((gchar*)tmp);
		g_free(tmp);
	} else {
		return;
	}

	if (!currentgame || currentgame->gameid != gameid) {
		game_free(gameid);
	}
}


void xmlparse_message(connection *c, xmlDocPtr doc, xmlNodePtr cur)  {

	gchar *type, *text, *author, *value;
	(void)c;
	(void)doc;

	type = (gchar*)xmlGetProp(cur, (xmlChar*)"type");

	if (!g_ascii_strncasecmp(type, "chat", 4)) {

		author = (gchar*)xmlGetProp(cur, (xmlChar*)"author");
		value = (gchar*)xmlGetProp(cur, (xmlChar*)"value");
		if(value && author)  {

			if(! strncmp("[ACTION]", value, 8) )
				text = g_strdup_printf("* %s%s", author, value + 8);
			else if(! strncmp("/me", value, 3) )
				text = g_strdup_printf("* %s%s", author, value + 3);
			else
				text = g_strdup_printf("<%s> %s", author, value);

			text_insert_chat(text, strlen(text));
			g_free(text);
		}
		if(value[0] == '!')  parse_specific_chat_message(value);
		if(author)  g_free(author);
		if(value)  g_free(value);
	}
	else {
		value = (gchar*)xmlGetProp(cur, (xmlChar*)"value");
		if (!g_ascii_strncasecmp(type, "error", 5)) {
			interface_set_infolabel(value, "b00000", false);
		} else {
			interface_set_infolabel(value, "808000", false);
		}
		g_free(value);
	}

	g_free(type);
}




void xmlparse_estateupdate(connection *c, xmlDocPtr doc, xmlNodePtr cur)  {

	guint32 color[3], i;
	xmlChar *tmp;
	eng_obj *pic;
	gint32 star, id, t, u;
	gboolean refresh = FALSE;
	gboolean owner_mortgage_changed = FALSE;
	(void)c;
	(void)doc;

	if (!currentgame) {
		return;
	}

	tmp = xmlGetProp(cur, (xmlChar*)"estateid");
	if(!tmp)  return;
	id = atoi( (gchar*)tmp );
	g_free(tmp);
	if(id < 0) return;

	tmp = xmlGetProp(cur, (xmlChar*)"name");
	if(tmp)  currentgame->estate[id].name = (gchar*)tmp;

	tmp = xmlGetProp(cur, (xmlChar*)"color");
	if(tmp) {

		sscanf((gchar*)tmp, "#%2X%2X%2X", &color[0], &color[1], &color[2]);
		currentgame->estate[id].color[0] = color[0];
		currentgame->estate[id].color[1] = color[1];
		currentgame->estate[id].color[2] = color[2];
		g_free(tmp);
	}

	tmp = xmlGetProp(cur, (xmlChar*)"bgcolor");
	if(tmp) {

		sscanf((gchar*)tmp, "#%2X%2X%2X", &color[0], &color[1], &color[2]);
		currentgame->estate[id].bgcolor[0] = color[0];
		currentgame->estate[id].bgcolor[1] = color[1];
		currentgame->estate[id].bgcolor[2] = color[2];
		g_free(tmp);
	}

	tmp = xmlGetProp(cur, (xmlChar*)"owner");
	if(tmp)  {

		t = currentgame->estate[id].owner = atoi((gchar*)tmp);
		g_free(tmp);
		owner_mortgage_changed = TRUE;
	}

	tmp = xmlGetProp(cur, (xmlChar*)"mortgaged");
	if(tmp)  {

		u = currentgame->estate[id].mortgaged = atoi((gchar*)tmp);
		g_free(tmp);
		owner_mortgage_changed = TRUE;
	}

	if(owner_mortgage_changed)  {
		gint32 cardid = get_playerlistcard_id_with_estate(id);

		t = currentgame->estate[id].owner;
		u = currentgame->estate[id].mortgaged;

		/* reset unowned cards of this estate to all players */
		if (cardid >= 0) {
			player *p;
			for (p = player_h; p; p = p->next) {
				if (p->spectator) continue;
				if (p->game != currentgame->gameid) continue;

				pic = p->playerlist_cards_pic[cardid];
				eng_pic_set_alpha(pic, data->playerlist_cards_alphaunowned);
			}
		}

		if(t > 0  &&  !u)  {

			/* star */
			star = player_from_id(t)->tokenid;

			pic = currentgame->estate[id].star_pic;
			eng_pic_set_width(pic, data->pngfile_star_width[star]);
			eng_pic_set_height(pic, data->pngfile_star_height[star]);
			eng_pic_set_pixbuf(pic, data->pngfile_star_buf[star]);
			eng_pic_show(pic);

			/* playerlist card */
			if (cardid >= 0) {
				pic = player_from_id(t)->playerlist_cards_pic[cardid];
			}

			eng_pic_set_bgcolor(pic, data->playerlist_cards_cardbgcolor);
			eng_pic_set_alpha(pic, data->playerlist_cards_alphaowned);
		}
		else  if(t > 0  &&  u)  {

			/* star */
			star = player_from_id(t)->tokenid;

			pic = currentgame->estate[id].star_pic;
			eng_pic_set_width(pic, data->pngfile_star_m_width[star]);
			eng_pic_set_height(pic, data->pngfile_star_m_height[star]);
			eng_pic_set_pixbuf(pic, data->pngfile_star_m_buf[star]);
			eng_pic_show(pic);

			/* playerlist card */
			if (cardid >= 0) {
				pic = player_from_id(t)->playerlist_cards_pic[cardid];
			}

			eng_pic_set_bgcolor(pic, data->playerlist_cards_cardbgcolormortgage);
			eng_pic_set_alpha(pic, data->playerlist_cards_alphamortgage);
		}
		else /* if( t <= 0 ) */ {

			/* star */
			eng_pic_unshow(currentgame->estate[id].star_pic);
		}

		/* update estatelist in trade panel */
		for(i = 0 ; i < MAX_TRADES ; i++)  {

			if(! currentgame->trade[i].open)  continue;
			if(currentgame->trade[i].current_component != TRADE_TYPE_ESTATE)  continue;

			trade_rebuild_subcomponent(i);
		}

		refresh = TRUE;
	}

	tmp = xmlGetProp(cur, (xmlChar*)"houses");
	if(tmp)  {

		t = currentgame->estate[id].houses = atoi((gchar*)tmp);
 		g_free(tmp);

		if(t <= 0)   {

			/* houses */
			pic = currentgame->estate[id].house_pic;
			eng_pic_unshow(pic);
		}
		else  if(t > 0  &&  data->estate[id].type_house == TYPE_HOUSE_HORIZONTAL)  {

			/* houses */
			pic = currentgame->estate[id].house_pic;
			eng_pic_set_width(pic, data->pngfile_horiz_house_width[t]);
			eng_pic_set_height(pic, data->pngfile_horiz_house_height[t]);
			eng_pic_set_pixbuf(pic, data->pngfile_horiz_house_buf[t]);
			eng_pic_show(pic);
		}
		else  if(t > 0  &&  data->estate[id].type_house == TYPE_HOUSE_VERTICAL)  {

			/* houses */
			pic = currentgame->estate[id].house_pic;
			eng_pic_set_width(pic, data->pngfile_vert_house_width[t]);
			eng_pic_set_height(pic, data->pngfile_vert_house_height[t]);
			eng_pic_set_pixbuf(pic, data->pngfile_vert_house_buf[t]);
			eng_pic_show(pic);
		}

		refresh = TRUE;
	}

	tmp = xmlGetProp(cur, (xmlChar*)"houseprice");
	if(tmp)  {
		currentgame->estate[id].houseprice = atoi((gchar*)tmp);
		g_free(tmp);
	}

	tmp = xmlGetProp(cur, (xmlChar*)"sellhouseprice");
	if(tmp)  {
		currentgame->estate[id].sellhouseprice = atoi((gchar*)tmp);
		g_free(tmp);
	}

	tmp = xmlGetProp(cur, (xmlChar*)"mortgageprice");
	if(tmp)  {
		currentgame->estate[id].mortgageprice = atoi((gchar*)tmp);
		g_free(tmp);
	}

	tmp = xmlGetProp(cur, (xmlChar*)"unmortgageprice");
	if(tmp)  {
		currentgame->estate[id].unmortgageprice = atoi((gchar*)tmp);
		g_free(tmp);
	}

	tmp = xmlGetProp(cur, (xmlChar*)"group");
	if(tmp)  {
		currentgame->estate[id].group = atoi((gchar*)tmp);
		g_free(tmp);
	}

	tmp = xmlGetProp(cur, (xmlChar*)"can_be_owned");
	if(tmp)  {
		currentgame->estate[id].can_be_owned = atoi((gchar*)tmp);
		g_free(tmp);
	}

	tmp = xmlGetProp(cur, (xmlChar*)"can_toggle_mortgage");
	if(tmp)  {
		currentgame->estate[id].can_toggle_mortgage = atoi((gchar*)tmp);
		g_free(tmp);
	}

	tmp = xmlGetProp(cur, (xmlChar*)"can_buy_houses");
	if(tmp)  {
		currentgame->estate[id].can_buy_houses = atoi((gchar*)tmp);
		g_free(tmp);
	}

	tmp = xmlGetProp(cur, (xmlChar*)"can_sell_houses");
	if(tmp)  {
		currentgame->estate[id].can_sell_houses = atoi((gchar*)tmp);
		g_free(tmp);
	}

	tmp = xmlGetProp(cur, (xmlChar*)"money");
	if(tmp)  {
		currentgame->estate[id].money = atoi((gchar*)tmp);
		g_free(tmp);
	}

	tmp = xmlGetProp(cur, (xmlChar*)"price");
	if(tmp)  {
		currentgame->estate[id].price = atoi((gchar*)tmp);
		g_free(tmp);
	}

	for(i = 0 ; i <= 5 ; i++) {

		xmlChar *key = (xmlChar*)g_strdup_printf("rent%d", i);
		tmp = xmlGetProp(cur, key);

		if(tmp)  {
			currentgame->estate[id].rent[i] = atoi((gchar*)tmp);
			g_free(tmp);
		}
		g_free(key);
	}

	if (refresh) {
		update_display();
	}
}




void xmlparse_playerupdate(connection *c, xmlDocPtr doc, xmlNodePtr cur)  {

	gint32 id, i;
	player *p;
	xmlChar *tmp;
	gboolean refresh = FALSE;
	(void)c;
	(void)doc;

	tmp = xmlGetProp(cur, (xmlChar*)"playerid");
	if(!tmp) return;
	id = atoi((gchar*)tmp);
	g_free(tmp);
	if(!id) return;

	/* create new player */
	p = player_from_id(id);
	if (p == NULL)  {
		p = game_new_player(id);
		if (!p) {
			return;
		}
	}

	tmp = xmlGetProp(cur, (xmlChar*)"game");
	if(tmp)  {
		p->game = atoi((gchar*)tmp);
		g_free(tmp);

		if (id == global->my_playerid) {
			if (p->game < 0) {
				if (currentgame) {
					game_quit();
				}
			} else {
				if (!currentgame) {
					game *game = game_find(p->game);
					if (game) {
						currentgame = game;
						printf("Current game: %d\n", game->gameid);
						game_switch_status();
					}
				}
			}
		} else if (p->game < 0) {
			interface_gameboard_remove_player(p);
			refresh = TRUE;
		}
	}

	tmp = xmlGetProp(cur, (xmlChar*)"host");
	if(tmp)  {
		if(p->host) g_free(p->host);
		p->host = (gchar*)tmp;
	}

	tmp = xmlGetProp(cur, (xmlChar*)"image");
	if(tmp)  {
		if(p->image) g_free(p->image);
		p->image = (gchar*)tmp;
	}

	tmp = xmlGetProp(cur, (xmlChar*)"name");
	if(tmp)  {
		if(p->name) g_free(p->name);
		p->name = (char*)tmp;

		if (currentgame && p->game == currentgame->gameid) {
			/* playerlist name */
			/* FIXME: handle hasturn color */
			if(p->playerlist_LabelNamePlayer)
				gtk_label_set_text(GTK_LABEL(p->playerlist_LabelNamePlayer), p->name);

			/* playerlist in trade panel */
			for(i = 0 ; i < MAX_TRADES ; i++)  {

				if(! currentgame->trade[i].open)  continue;

				trade_rebuild_playerlist(i);
			}

			/* update sub component players in trade panel */
			for(i = 0 ; i < MAX_TRADES ; i++)  {

				if(! currentgame->trade[i].open)  continue;

				trade_rebuild_subcomponent(i);
			}
		}
	}

	tmp = xmlGetProp(cur, (xmlChar*)"money");
	if(tmp)  {
		p->money = atoi((gchar*)tmp);
		g_free(tmp);

		if (currentgame && p->game == currentgame->gameid) {
			if(p->playerlist_LabelMoneyPlayer) {
				if(p->hasturn) {
					gchar *tmp = g_markup_printf_escaped("<span foreground=\"#ff0000\">%d</span>", p->money);
					gtk_label_set_markup(GTK_LABEL(p->playerlist_LabelMoneyPlayer), tmp);
					g_free(tmp);
				} else {
					gchar *tmp = g_markup_printf_escaped("<span foreground=\"#000000\">%d</span>", p->money);
					gtk_label_set_markup(GTK_LABEL(p->playerlist_LabelMoneyPlayer), tmp);
					g_free(tmp);
				}
			}
		}
	}

	tmp = xmlGetProp(cur, (xmlChar*)"location");
	if(tmp)  {
		p->location_to = atoi((gchar*)tmp);
		g_free(tmp);

		tmp = xmlGetProp(cur, (xmlChar*)"directmove");
		if (tmp) {
			p->directmove = atoi((gchar*)tmp);
			g_free(tmp);
		}

		if (currentgame && p->game == currentgame->gameid) {
			refresh = TRUE;
		}
	}

	tmp = xmlGetProp(cur, (xmlChar*)"jailed");
	if(tmp)  {
		gboolean jailed = atoi((gchar*)tmp);
		g_free(tmp);

		if (jailed != p->jailed) {
			p->jailed = jailed;
			p->directmove = TRUE; /* force directmove when player is going to or leaving jail */
		}

		if (currentgame && p->game == currentgame->gameid) {
			refresh = TRUE;
		}
	}

	tmp = xmlGetProp(cur, (xmlChar*)"bankrupt");
	if(tmp)  {
		p->bankrupt = atoi((gchar*)tmp);
		g_free(tmp);

		if (currentgame && p->game == currentgame->gameid) {
			/* remove token */
			if(p->bankrupt) {
				eng_pic_unshow(p->token_pic);
				refresh = TRUE;
			}
		}
	}

	tmp = xmlGetProp(cur, (xmlChar*)"hasturn");
	if(tmp)  {
		p->hasturn = atoi((gchar*)tmp);
		g_free(tmp);

		if (currentgame && p->game == currentgame->gameid) {
			/* set has turn attributes */
			if(p->hasturn)  {

				if (p->playerlist_LabelNamePlayer)  {
					gchar *tmp = g_markup_printf_escaped("<span foreground=\"#ff0000\">%s</span>", p->name);
					gtk_label_set_markup(GTK_LABEL(p->playerlist_LabelNamePlayer), tmp);
					g_free(tmp);

					tmp = g_markup_printf_escaped("<span foreground=\"#ff0000\">%d</span>", p->money);
					gtk_label_set_markup(GTK_LABEL(p->playerlist_LabelMoneyPlayer), tmp);
					g_free(tmp);
				}

				eng_pic_unset_alpha(p->token_pic);
			}
			else  {

				if (p->playerlist_LabelNamePlayer)  {
					gchar *tmp = g_markup_printf_escaped("<span foreground=\"#000000\">%s</span>", p->name);
					gtk_label_set_markup(GTK_LABEL(p->playerlist_LabelNamePlayer), tmp);
					g_free(tmp);

					tmp = g_markup_printf_escaped("<span foreground=\"#000000\">%d</span>", p->money);
					gtk_label_set_markup(GTK_LABEL(p->playerlist_LabelMoneyPlayer), tmp);
					g_free(tmp);
				}

				if(config->game_token_transparency)
					eng_pic_set_alpha(p->token_pic, 0x7f);
			}

			refresh = TRUE;
		}
	}

	tmp = xmlGetProp(cur, (xmlChar*)"can_roll");
	if(tmp)  {
		p->can_roll = atoi((gchar*)tmp);
		g_free(tmp);
	}

	tmp = xmlGetProp(cur, (xmlChar*)"can_buyestate");
	if(tmp)  {
		p->can_buyestate = atoi((gchar*)tmp);
		g_free(tmp);
	}

	tmp = xmlGetProp(cur, (xmlChar*)"turnorder");
	if(tmp)  {
		p->turnorder = atoi((gchar*)tmp);
		g_free(tmp);
	}

	tmp = xmlGetProp(cur, (xmlChar*)"spectator");
	if(tmp)  {
		p->spectator = atoi((gchar*)tmp);
		g_free(tmp);
		if (currentgame && p->game == currentgame->gameid && !p->spectator) {
			/* note: turnorder must be fetched before */
			game_sort_playerlist_by_turnorder();
			interface_gameboard_add_player(p);
			game_update_tokens();
		}
	}

	game_buildplayerlist();
	if (refresh) {
		game_initiate_token_movement();
		update_display();
	}
}




void xmlparse_auctionupdate(connection *c, xmlDocPtr doc, xmlNodePtr cur)  {

	guint32 auctionid, highbid = 0;
	xmlChar *tmp;
	(void)c;
	(void)doc;

	if (!currentgame || currentgame->status != GAME_STATUS_RUN) {
		return;
	}

	tmp = xmlGetProp(cur, (xmlChar*)"auctionid");
	if(!tmp) return;
	auctionid = atoi((gchar*)tmp);
	g_free(tmp);

	/* initialise auction */
	tmp = xmlGetProp(cur, (xmlChar*)"estateid");
	if (tmp && g_object_get_data(G_OBJECT(currentgame->BoardCenter), "auctionbox") == NULL) {
		guint32 estateid = atoi((gchar*)tmp);
		g_free(tmp);

		display_hide();
		interface_create_auctionbox(auctionid, estateid);
		g_object_set_data(G_OBJECT(currentgame->BoardCenter), "auction_highbid", 0);
	}

	if (g_object_get_data(G_OBJECT(currentgame->BoardCenter), "auctionbox") == NULL) {
		return;
	}

	/* bid */
	tmp = xmlGetProp(cur, (xmlChar*)"highbid");
	if(tmp) {
		highbid = atoi((gchar*)tmp);
		g_object_set_data(G_OBJECT(currentgame->BoardCenter), "auction_highbid", GINT_TO_POINTER(highbid));
		g_free(tmp);
	}

	tmp = xmlGetProp(cur, (xmlChar*)"highbidder");
	if(tmp) {
		guint32 highbidder = atoi((gchar*)tmp);
		g_free(tmp);

		GtkListStore *store;
		GtkTreeIter iter;
		gboolean valid;

		store = g_object_get_data(G_OBJECT(currentgame->BoardCenter), "auction_store");
		valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(store), &iter);
		while (valid) {
			guint32 eplayerid;

			gtk_tree_model_get(GTK_TREE_MODEL(store), &iter, AUCTIONPLAYERLIST_COLUMN_PLAYERID, &eplayerid, -1);
			if (eplayerid == highbidder)  {
				gtk_list_store_set(store, &iter,
					AUCTIONPLAYERLIST_COLUMN_BID, highbid,
					AUCTIONPLAYERLIST_WEIGHT, PANGO_WEIGHT_BOLD,
					-1);
			} else {
				gtk_list_store_set(store, &iter,
					AUCTIONPLAYERLIST_WEIGHT, PANGO_WEIGHT_NORMAL,
					-1);
			}

			valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(store), &iter);
		}
	}

	/* status */
	tmp = xmlGetProp(cur, (xmlChar*)"status");
	if(tmp) {
		guint32 status = atoi((gchar*)tmp);
		g_free(tmp);

		GtkWidget *label = g_object_get_data(G_OBJECT(currentgame->BoardCenter), "auction_statuslabel");

		switch (status) {
			case 0:
				gtk_label_set_text(GTK_LABEL(label), NULL);
				break;

			case 1:
				gtk_label_set_text(GTK_LABEL(label), "Going once…");
				break;

			case 2:
				gtk_label_set_text(GTK_LABEL(label), "Going twice…");
				break;

			case 3:
				gtk_widget_destroy(g_object_get_data(G_OBJECT(currentgame->BoardCenter), "auctionbox"));
				g_object_set_data(G_OBJECT(currentgame->BoardCenter), "auctionbox", NULL);
				display_show();
				break;
		}
	}
}




void xmlparse_display(connection *c, xmlDocPtr doc, xmlNodePtr cur)  {

	xmlNodePtr cur2;
	gint32 estateid;
	xmlChar *tmp;
	gchar *caption, *command;
	gboolean enable;
	(void)c;
	(void)doc;

	if (!currentgame || currentgame->status < GAME_STATUS_INIT) {
		return;
	}

	tmp = xmlGetProp(cur, (xmlChar*)"cleartext");
	if(tmp)  {
		if( atoi((gchar*)tmp) )  display_clear_text();
		g_free(tmp);
	}

	tmp = xmlGetProp(cur, (xmlChar*)"clearbuttons");
	if(tmp)  {
		if( atoi((gchar*)tmp) )  display_clear_buttons();
		g_free(tmp);
	}

	tmp = xmlGetProp(cur, (xmlChar*)"estateid");
	if(tmp)   {
		estateid = atoi((gchar*)tmp);
		g_free(tmp);
		display_estate(estateid);
	}

	tmp = xmlGetProp(cur, (xmlChar*)"text");
	if(tmp)  {

		if( strlen((gchar*)tmp) > 0)  display_text((gchar*)tmp);
		g_free(tmp);
	}

	/* buttons */
	for(cur2 = cur->xmlChildrenNode ; cur2 != NULL ; cur2 = cur2 -> next)  {

		if(! xmlStrcmp(cur2->name, (xmlChar*)"button") )  {

			caption = (gchar*)xmlGetProp(cur2, (xmlChar*)"caption");
			command = (gchar*)xmlGetProp(cur2, (xmlChar*)"command");

			tmp = xmlGetProp(cur2, (xmlChar*)"enabled");
			enable = atoi((gchar*)tmp);
			g_free(tmp);

			display_add_button((gchar*)caption, (gchar*)command, enable);
			g_free(caption);
			g_free(command);
		}
	}
}




void xmlparse_configupdate(connection *c, xmlDocPtr doc, xmlNodePtr cur)  {

	GtkWidget *CButton;
	gchar *description;
	guint32 signalid;
	xmlChar *tmp;
	game *game;
	gint32 id;
	(void)c;
	(void)doc;

	tmp = xmlGetProp(cur, (xmlChar*)"gameid");
	if (!tmp) {
		return;
	}

	game = game_find(atoi((gchar*)tmp));
	g_free(tmp);
	if (!game) {
		return;
	}

	tmp = xmlGetProp(cur, (xmlChar*)"configid");
	if (!tmp) {
		return;
	}

	id = atoi((gchar*)tmp);
	g_free(tmp);
	if (id <= 0) {
		return;
	}

	/* We are going to receive <configupdate/> events before we know in which game we are
	 * therefore we create the config box before the config panel is created
	 */
	if (!game->GameConfigBox) {
		game->GameConfigBox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	}

	/* create it if not created */
	gchar *strid = g_strdup_printf("%d", id);
	if ( !(CButton = g_object_get_data(G_OBJECT(game->GameConfigBox), strid)) ) {

		/* We only handle boolean type for now */
		tmp = xmlGetProp(cur, (xmlChar*)"type");
		if (!tmp) {
			goto abort;
		}
		if(xmlStrcmp(tmp, (xmlChar*)"bool") )  {
			g_free(tmp);
			goto abort;
		}
		g_free(tmp);

		description = (gchar*)xmlGetProp(cur, (xmlChar*)"description");
		if (!description) {
			goto abort;
		}

		CButton = gtk_check_button_new_with_label(description);
		signalid = g_signal_connect(G_OBJECT(CButton), "toggled", G_CALLBACK(Callback_toggle_boolean_gameoption), NULL);
		gtk_widget_set_sensitive(CButton, (global->my_playerid == game->master));
		gtk_box_pack_start(GTK_BOX(game->GameConfigBox), CButton, FALSE, FALSE, 0);

		/* store signal identifier */
		g_object_set_data(G_OBJECT(CButton), "signal", GINT_TO_POINTER(signalid));

		/* store id under CButton */
		g_object_set_data(G_OBJECT(CButton), "id", GINT_TO_POINTER(id));

		/* save the pointer of button to use in the future */
		g_object_set_data(G_OBJECT(game->GameConfigBox), strid, CButton);

		g_free(description);
	}

	/* modify */
	tmp = xmlGetProp(cur, (xmlChar*)"value");
	if (tmp) {

		/* disconnect signal */
		signalid = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(CButton), "signal"));
		g_signal_handler_disconnect(G_OBJECT(CButton), signalid);

		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(CButton), (atoi((gchar*)tmp) != 0));
		g_free(tmp);

		/* reconnect signal */
		signalid = g_signal_connect(G_OBJECT(CButton), "toggled", G_CALLBACK(Callback_toggle_boolean_gameoption), NULL);
		g_object_set_data(G_OBJECT(CButton), "signal", GINT_TO_POINTER(signalid));
	}

/*	tmp = xmlGetProp(cur, (xmlChar*)"edit");   USELESS */

abort:
	g_free(strid);
	gtk_widget_show_all(game->GameConfigBox);
}




void xmlparse_tradeupdate(connection *c, xmlDocPtr doc, xmlNodePtr cur)  {

	xmlNodePtr cur2;
	xmlChar *tmp;
	guint32 tradeid = 0, playerid = 0, cardid = 0, estateid = 0;
	guint32 targetplayer = 0, playerfrom = 0, playerto = 0, money = 0;
	gboolean accept = FALSE;
	(void)c;
	(void)doc;

	if (!currentgame || currentgame->status != GAME_STATUS_RUN) {
		return;
	}

	tmp = xmlGetProp(cur, (xmlChar*)"tradeid");
	if(tmp)  {
		tradeid = atoi((gchar*)tmp);
		g_free(tmp);
	}
	else  return;

//	tmp = xmlGetProp(cur, (xmlChar*)"actor");
//	if(tmp)   {
//		actor = atoi((gchar*)tmp);
//		g_free(tmp);
//	}

	tmp = xmlGetProp(cur, (xmlChar*)"type");
	if(tmp) {
		if (!xmlStrcmp(tmp, (xmlChar*)"rejected") || !xmlStrcmp(tmp, (xmlChar*)"accepted") || !xmlStrcmp(tmp, (xmlChar*)"completed")) {
			trade_destroy(tradeid);
			g_free(tmp);
			return;
		}

		if (!xmlStrcmp(tmp, (xmlChar*)"new")) {
			trade_initnew(tradeid);
		}

		g_free(tmp);
	}

	tmp = xmlGetProp(cur, (xmlChar*)"revision");
	if (tmp) {
		trade_update_revision(tradeid, atoi((gchar*)tmp));
		g_free(tmp);
	}

	for(cur2 = cur->xmlChildrenNode ; cur2 != NULL ; cur2 = cur2 -> next)  {

		/* -- player -- */
		if(! xmlStrcmp(cur2->name, (xmlChar*)"tradeplayer") )  {

			tmp = xmlGetProp(cur2, (xmlChar*)"playerid");
			if(tmp)   {
				playerid = atoi((gchar*)tmp);
				g_free(tmp);
			}

			tmp = xmlGetProp(cur2, (xmlChar*)"accept");
			if(tmp)   {
				accept = atoi((gchar*)tmp);
				g_free(tmp);
			}

			trade_update_player(tradeid, playerid, accept);
		}

		/* -- card -- */
		if(! xmlStrcmp(cur2->name, (xmlChar*)"tradecard") )  {

			tmp = xmlGetProp(cur2, (xmlChar*)"cardid");
			if(tmp)   {
				cardid = atoi((gchar*)tmp);
				g_free(tmp);
			}

			tmp = xmlGetProp(cur2, (xmlChar*)"targetplayer");
			if(tmp)   {
				targetplayer = atoi((gchar*)tmp);
				g_free(tmp);
			}

			trade_update_card(tradeid, cardid, targetplayer);
		}

		/* -- estate -- */
		if(! xmlStrcmp(cur2->name, (xmlChar*)"tradeestate") )  {

			tmp = xmlGetProp(cur2, (xmlChar*)"estateid");
			if(tmp)   {
				estateid = atoi((gchar*)tmp);
				g_free(tmp);
			}

			tmp = xmlGetProp(cur2, (xmlChar*)"targetplayer");
			if(tmp)   {
				targetplayer = atoi((gchar*)tmp);
				g_free(tmp);
			}

			trade_update_estate(tradeid, estateid, targetplayer);
		}

		/* -- money -- */
		if(! xmlStrcmp(cur2->name, (xmlChar*)"trademoney") )  {

			tmp = xmlGetProp(cur2, (xmlChar*)"playerfrom");
			if(tmp)   {
				playerfrom = atoi((gchar*)tmp);
				g_free(tmp);
			}

			tmp = xmlGetProp(cur2, (xmlChar*)"playerto");
			if(tmp)   {
				playerto = atoi((gchar*)tmp);
				g_free(tmp);
			}

			tmp = xmlGetProp(cur2, (xmlChar*)"money");
			if(tmp)   {
				money = atoi((gchar*)tmp);
				g_free(tmp);
			}

			trade_update_money(tradeid, playerfrom, playerto, money);
		}
	}

}




void xmlparse_cardupdate(connection *c, xmlDocPtr doc, xmlNodePtr cur)  {

	xmlChar *tmp;
	gint32 cardid, owner, i, cardslot;
	(void)c;
	(void)doc;

	if (!currentgame || currentgame->status < GAME_STATUS_INIT) {
		return;
	}

	tmp = xmlGetProp(cur, (xmlChar*)"cardid");
	if (!tmp) {
		return;
	}
	cardid = atoi((gchar*)tmp);
	g_free(tmp);

	tmp = xmlGetProp(cur, (xmlChar*)"owner");
	if (!tmp) {
		return;
	}
	owner = atoi((gchar*)tmp);
	g_free(tmp);

	cardslot = get_card_slot_with_cardid(cardid);

	/* create new or update card slot */
	if (owner >= 0)  {
		if (cardslot < 0 && !game_get_valid_card_slot(&cardslot)) {
			return;
		}

		currentgame->card[cardslot].cardid = cardid;
		currentgame->card[cardslot].owner = owner;

		xmlChar *title = xmlGetProp(cur, (xmlChar*)"title");
		if (title) {
			if (currentgame->card[cardslot].title) {
				g_free(currentgame->card[cardslot].title);
			}
			currentgame->card[cardslot].title = (gchar*)title;
		}
	}

	/* destroy a card slot */
	else {
		if (cardslot < 0) {
			return;
		}

		currentgame->card[cardslot].cardid = 0;
		currentgame->card[cardslot].owner = 0;
		if (currentgame->card[cardslot].title) {
			g_free(currentgame->card[cardslot].title);
			currentgame->card[cardslot].title = NULL;
		}

	}

	/* update component list in trade panel */
	for(i = 0 ; i < MAX_TRADES ; i++)  {

		if(! currentgame->trade[i].open)  continue;

		trade_rebuild_component(i);
		trade_rebuild_subcomponent(i);
	}
}




void xmlparse_estategroupupdate(connection *c, xmlDocPtr doc, xmlNodePtr cur)  {

	xmlChar *tmp;
	gint32 groupid = -1;
	(void)c;
	(void)doc;

	if (!currentgame) {
		return;
	}

	tmp = xmlGetProp(cur, (xmlChar*)"groupid");
	if(tmp)  {
		groupid = atoi((gchar*)tmp);
		g_free(tmp);
	}

	if(groupid < 0)  return;

	if(currentgame->group[groupid].name)  g_free(currentgame->group[groupid].name);

	currentgame->group[groupid].name = (gchar*)xmlGetProp(cur, (xmlChar*)"name");
}




void xmlparse_deleteplayer(connection *c, xmlDocPtr doc, xmlNodePtr cur)  {

	xmlChar *tmp;
	gint32 id;
	player *p;
	(void)c;
	(void)doc;

	tmp = xmlGetProp(cur, (xmlChar*)"playerid");
	if (!tmp) return;
	id = atoi((gchar*)tmp);
	g_free(tmp);

	if (id < 0) return;

	p = player_from_id(id);
	if (p == NULL) return;

	interface_gameboard_remove_player(p);
	update_display();
	game_free_player(p);
	game_buildplayerlist();
}




void xmlparse_gamelist_gameupdate(connection *c, xmlDocPtr doc, xmlNodePtr cur) {
	xmlChar *tmp;
	gchar *players = NULL;
	gint32 id;
	GtkTreeIter iter;
	gboolean valid;
	gboolean update = FALSE;
	gchar *status = NULL;
	gint32 canbejoined = -1, canbewatched = -1;
	(void)doc;

	tmp = xmlGetProp(cur, (xmlChar*)"gameid");
	id = atoi((gchar*)tmp);
	g_free(tmp);

	/* lookup if gameid on same server already exist */
	if (id > 0) {
		valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(global->game_store), &iter);
		while (valid) {
			gint32 source;
			gint32 eid;

			gtk_tree_model_get(GTK_TREE_MODEL(global->game_store), &iter, GAMELIST_COLUMN_GAMEID, &eid, GAMELIST_COLUMN_SOURCE, &source, -1);
			if (eid == id && source == GAMELIST_SOURCE_CUSTOM)  {
				update = TRUE;
				break;
			}

			valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(global->game_store), &iter);
		}
	}

	/* ONLY TEMPORARY ! */
	tmp = xmlGetProp(cur, (xmlChar*)"gametype");
	if (tmp && xmlStrcmp(tmp, (xmlChar*)"city") && xmlStrcmp(tmp, (xmlChar*)"french_city") )  {
		g_free(tmp);
		return;
	}

	tmp = xmlGetProp(cur, (xmlChar*)"turn");
	if(tmp) {
		int turn = atoi((gchar*)tmp);
		g_free(tmp);

		if (turn >= 1) {
			status = g_strdup_printf("Playing, turn %d", turn);
		}
	}

	tmp = xmlGetProp(cur, (xmlChar*)"status");
	if(tmp) {
		if (!xmlStrcmp(tmp, (xmlChar*)"config") ) {
			if (status) {
				g_free(status);
			}
			status = g_strdup("Waiting for players");
		}
		else if (!xmlStrcmp(tmp, (xmlChar*)"end") ) {
			if (status) {
				g_free(status);
			}
			status = g_strdup("Finished");
		}

		g_free(tmp);
	}

	// game template
	if (id < 0) {
		canbejoined = true;
		canbewatched = false;
	}
	tmp = xmlGetProp(cur, (xmlChar*)"canbejoined");
	if (tmp) {
		canbejoined = atoi((gchar*)tmp);
		g_free(tmp);
	}

	tmp = xmlGetProp(cur, (xmlChar*)"canbewatched");
	if (tmp) {
		canbewatched = atoi((gchar*)tmp);
		g_free(tmp);
	}

	players = (gchar*)xmlGetProp(cur, (xmlChar*)"players");

	if (update) {
		if (status) {
			gtk_list_store_set(global->game_store, &iter,
			GAMELIST_COLUMN_STATUS, status,
			-1);
		}

		if (players) {
			gtk_list_store_set(global->game_store, &iter,
			GAMELIST_COLUMN_PLAYERS, players,
			-1);
		}

		if (canbejoined >= 0) {
			gtk_list_store_set(global->game_store, &iter,
			GAMELIST_COLUMN_CANBEJOINED, canbejoined,
			-1);
		}

		if (canbewatched >= 0) {
			gtk_list_store_set(global->game_store, &iter,
			GAMELIST_COLUMN_CANBEWATCHED, canbewatched,
			-1);
		}

		if (canbejoined >= 0 || canbewatched >= 0) {
			gint32 joined, watched;

			gtk_tree_model_get(GTK_TREE_MODEL(global->game_store), &iter, GAMELIST_COLUMN_CANBEJOINED, &joined, GAMELIST_COLUMN_CANBEWATCHED, &watched, -1);
			gtk_list_store_set(global->game_store, &iter,
			GAMELIST_COLUMN_BGCOLOR, (joined || watched) ? global->bg_green : global->bg_red,
			-1);
		}
	}
	else {
		gchar *game = NULL;
		gchar *gametype = (gchar*)xmlGetProp(cur, (xmlChar*)"gametype");

		tmp = xmlGetProp(cur, (xmlChar*)"name");
		if (tmp) {
			if (id < 0) {
				game = g_strdup_printf("Create new %s game", (gchar*)tmp);
			} else {
				game = g_strdup_printf("%s game %d", (gchar*)tmp, id);
			}
			g_free(tmp);
		}

		gtk_list_store_append(global->game_store, &iter);
		gtk_list_store_set(global->game_store, &iter,
		GAMELIST_COLUMN_HOST, c->host,
		GAMELIST_COLUMN_VERSION, c->server_version,
		GAMELIST_COLUMN_GAME, game,
		GAMELIST_COLUMN_STATUS, status,
		GAMELIST_COLUMN_PLAYERS, players,
		GAMELIST_COLUMN_PORT, c->port,
		GAMELIST_COLUMN_GAMETYPE, gametype,
		GAMELIST_COLUMN_GAMEID, id,
		GAMELIST_COLUMN_CANBEJOINED, canbejoined,
		GAMELIST_COLUMN_CANBEWATCHED, canbewatched,
		GAMELIST_COLUMN_BGCOLOR, (canbejoined || canbewatched) ? global->bg_green : global->bg_red,
		GAMELIST_COLUMN_SOURCE, GAMELIST_SOURCE_CUSTOM,
		-1);

		g_free(gametype);
		g_free(game);
	}

	if(players) g_free(players);
	if(status) g_free(status);
}


void xmlparse_gamelist_deletegame(connection *c, xmlDocPtr doc, xmlNodePtr cur) {
	xmlChar *tmp;
	gint32 id;
	GtkTreeIter iter;
	gboolean valid;
	(void)doc;
	(void)c;

	tmp = xmlGetProp(cur, (xmlChar*)"gameid");
	id = atoi((gchar*)tmp);
	g_free(tmp);

	valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(global->game_store), &iter);
	while (valid) {
		gint32 source;
		gint32 eid;
		GtkTreeIter curiter = iter;

		valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(global->game_store), &iter); /* get next iter so we can remove the current entry safely */

		gtk_tree_model_get(GTK_TREE_MODEL(global->game_store), &curiter, GAMELIST_COLUMN_GAMEID, &eid, GAMELIST_COLUMN_SOURCE, &source, -1);
		if (eid == id && source == GAMELIST_SOURCE_CUSTOM)  {
			gtk_list_store_remove(global->game_store, &curiter);
		}
	}
}


void xmlparse_metaserver(connection *c, gchar *buffer) {

	xmlChar *tmp;
	xmlDocPtr doc;
	xmlNodePtr cur, cur2;
	(void)c;

	doc = xmlParseMemory(buffer,  strlen(buffer) );
	if(doc == NULL)  return;

	cur = xmlDocGetRootElement(doc);
	if (cur == NULL) {
		xmlFreeDoc(doc);
		return;
	}

	if( xmlStrcmp(cur->name, METASERVER_XMLROOTELEMENT) )  {
		xmlFreeDoc(doc);
		return;
	}

	for(cur = cur->xmlChildrenNode ; cur != NULL ; cur = cur -> next)  {

		if(! xmlStrcmp(cur->name, (xmlChar*)"metaserver") )  {
			gchar *text;

			tmp = xmlGetProp(cur, (xmlChar*)"version");
			text = g_strdup_printf("Atlantic metaserver version %s", tmp);
			g_free(tmp);
			interface_set_infolabel(text, "008000", false);
			g_free(text);
		}

		if(! xmlStrcmp(cur->name, (xmlChar*)"msg") )  {
			gchar *text;

			tmp = xmlGetProp(cur, (xmlChar*)"value");
			text = g_strdup_printf("%s", tmp);
			g_free(tmp);
			interface_create_messagewin(text);
			g_free(text);
		}

		if(!xmlStrcmp(cur->name, (xmlChar*)"server") )  {
			gint32 serverid, users = -1;
			GtkTreeIter serveriter;
			bool valid, update = false;

			tmp = xmlGetProp(cur, (xmlChar*)"id");
			if (!tmp) {
				continue;
			}
			serverid = atoi((gchar*)tmp);
			g_free(tmp);

			/* lookup if server already exist */
			valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(global->server_store), &serveriter);
			while (valid) {
				gint32 eid;

				gtk_tree_model_get(GTK_TREE_MODEL(global->server_store), &serveriter, SERVERLIST_COLUMN_SERVERID, &eid, -1);
				if (eid == serverid) {
					update = true;
					break;
				}

				valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(global->server_store), &serveriter);
			}

			tmp = xmlGetProp(cur, (xmlChar*)"users");
			if (tmp) {
				users = atoi((gchar*)tmp);
				g_free(tmp);

				if (update == true) {
					gtk_list_store_set(global->server_store, &serveriter,
					  SERVERLIST_COLUMN_USERS, users,
					  -1);
				}
			}

			if (update == false) {
				gchar *host, *version;
				gint32 port = -1;

				host = (gchar*)xmlGetProp(cur, (xmlChar*)"host");

				tmp = xmlGetProp(cur, (xmlChar*)"port");
				if (tmp) {
					port = atoi((gchar*)tmp);
					g_free(tmp);
				}
				version = (gchar*)xmlGetProp(cur, (xmlChar*)"version");

				if (! (host && version && port >= 0 && users >= 0)) {
					continue;
				}

				gtk_list_store_append(global->server_store, &serveriter);
				gtk_list_store_set(global->server_store, &serveriter,
				  SERVERLIST_COLUMN_HOST, host,
				  SERVERLIST_COLUMN_PORT, port,
				  SERVERLIST_COLUMN_VERSION, version,
				  SERVERLIST_COLUMN_USERS, users,
				  SERVERLIST_COLUMN_SERVERID, serverid,
				  -1);

				g_free(host);
				g_free(version);
			}

			for(cur2 = cur->xmlChildrenNode ; cur2 != NULL ; cur2 = cur2 -> next )  {

				if (!xmlStrcmp(cur2->name, (xmlChar*)"game") )  {
					gchar *players;
					gint32 gameid;
					GtkTreeIter iter;
					bool valid, update = false;
					gchar *status = NULL;
					gint32 canbejoined = -1, canbewatched = -1;

					/* ONLY TEMPORARY ! */
					tmp = xmlGetProp(cur2, (xmlChar*)"gametype");
					if (tmp && xmlStrcmp(tmp, (xmlChar*)"city") && xmlStrcmp(tmp, (xmlChar*)"french_city") )  {
						g_free(tmp);
						continue;
					}

					tmp = xmlGetProp(cur2, (xmlChar*)"id");
					if (!tmp) {
						continue;
					}
					gameid = atoi((gchar*)tmp);
					g_free(tmp);

					/* lookup if gameid on same server already exist */
					if (gameid > 0) {
						valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(global->game_store), &iter);
						while (valid) {
							gint32 source;
							gint32 egameid, eserverid;

							gtk_tree_model_get(GTK_TREE_MODEL(global->game_store), &iter, GAMELIST_COLUMN_GAMEID, &egameid, GAMELIST_COLUMN_SERVERID, &eserverid, GAMELIST_COLUMN_SOURCE, &source, -1);
							if (source == GAMELIST_SOURCE_METASERVER && egameid == gameid && eserverid == serverid)  {
								update = true;
								break;
							}

							valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(global->game_store), &iter);
						}
					}

					tmp = xmlGetProp(cur2, (xmlChar*)"turn");
					if(tmp) {
						int turn = atoi((gchar*)tmp);
						g_free(tmp);

						if (turn >= 1) {
							status = g_strdup_printf("Playing, turn %d", turn);
						}
					}

					tmp = xmlGetProp(cur2, (xmlChar*)"status");
					if(tmp) {
						if (!xmlStrcmp(tmp, (xmlChar*)"config") ) {
							if (status) {
								g_free(status);
							}
							status = g_strdup("Waiting for players");
						}
						else if (!xmlStrcmp(tmp, (xmlChar*)"end") ) {
							if (status) {
								g_free(status);
							}
							status = g_strdup("Finished");
						}

						g_free(tmp);
					}

					// game template
					if (gameid < 0) {
						canbejoined = true;
						canbewatched = false;
					}
					tmp = xmlGetProp(cur2, (xmlChar*)"canbejoined");
					if (tmp) {
						canbejoined = atoi((gchar*)tmp);
						g_free(tmp);
					}

					tmp = xmlGetProp(cur2, (xmlChar*)"canbewatched");
					if (tmp) {
						canbewatched = atoi((gchar*)tmp);
						g_free(tmp);
					}

					players = (gchar*)xmlGetProp(cur2, (xmlChar*)"players");

					if (update) {
						if (status) {
							gtk_list_store_set(global->game_store, &iter,
							GAMELIST_COLUMN_STATUS, status,
							-1);
						}

						if (players) {
							gtk_list_store_set(global->game_store, &iter,
							GAMELIST_COLUMN_PLAYERS, players,
							-1);
						}

						if (canbejoined >= 0) {
							gtk_list_store_set(global->game_store, &iter,
							GAMELIST_COLUMN_CANBEJOINED, canbejoined,
							-1);
						}

						if (canbewatched >= 0) {
							gtk_list_store_set(global->game_store, &iter,
							GAMELIST_COLUMN_CANBEWATCHED, canbewatched,
							-1);
						}

						if (canbejoined >= 0 || canbewatched >= 0) {
							gint32 joined, watched;

							gtk_tree_model_get(GTK_TREE_MODEL(global->game_store), &iter, GAMELIST_COLUMN_CANBEJOINED, &joined, GAMELIST_COLUMN_CANBEWATCHED, &watched, -1);
							gtk_list_store_set(global->game_store, &iter,
							GAMELIST_COLUMN_BGCOLOR, (joined || watched) ? global->bg_green : global->bg_red,
							-1);
						}
					}
					else {
						gchar *host, *version, *game, *gametype;
						gint32 port;

						tmp = xmlGetProp(cur2, (xmlChar*)"name");
						if (!tmp) {
							continue;
						}
						if(gameid < 0) {
							game = g_strdup_printf("Create new %s game", (gchar*)tmp);
						} else {
							game = g_strdup_printf("%s game %d", (gchar*)tmp, gameid);
						}
						g_free(tmp);

						gametype = (gchar*)xmlGetProp(cur2, (xmlChar*)"gametype");

						gtk_tree_model_get(GTK_TREE_MODEL(global->server_store), &serveriter,
						  SERVERLIST_COLUMN_HOST, &host,
						  SERVERLIST_COLUMN_PORT, &port,
						  SERVERLIST_COLUMN_VERSION, &version,
						  -1);

						gtk_list_store_append(global->game_store, &iter);
						gtk_list_store_set(global->game_store, &iter,
						  GAMELIST_COLUMN_HOST, host,
						  GAMELIST_COLUMN_VERSION, version,
						  GAMELIST_COLUMN_GAME, game,
						  GAMELIST_COLUMN_STATUS, status,
						  GAMELIST_COLUMN_PLAYERS, players,
						  GAMELIST_COLUMN_PORT, port,
						  GAMELIST_COLUMN_GAMETYPE, gametype,
						  GAMELIST_COLUMN_GAMEID, gameid,
						  GAMELIST_COLUMN_SERVERID, serverid,
						  GAMELIST_COLUMN_CANBEJOINED, canbejoined,
						  GAMELIST_COLUMN_CANBEWATCHED, canbewatched,
						  GAMELIST_COLUMN_BGCOLOR, (canbejoined || canbewatched) ? global->bg_green : global->bg_red,
						  GAMELIST_COLUMN_SOURCE, GAMELIST_SOURCE_METASERVER,
						  -1);

						g_free(host);
						g_free(version);
						g_free(game);
						g_free(gametype);
					}

					if (players) g_free(players);
					if (status) g_free(status);
				}
				else if (!xmlStrcmp(cur2->name, (xmlChar*)"deletegame") )  {
					gint32 gameid;
					GtkTreeIter iter;
					bool valid;

					tmp = xmlGetProp(cur2, (xmlChar*)"id");
					if (!tmp) {
						continue;
					}
					gameid = atoi((gchar*)tmp);
					g_free(tmp);

					valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(global->game_store), &iter);
					while (valid) {
						gint32 source;
						gint32 egameid, eserverid;

						gtk_tree_model_get(GTK_TREE_MODEL(global->game_store), &iter, GAMELIST_COLUMN_GAMEID, &egameid, GAMELIST_COLUMN_SERVERID, &eserverid, GAMELIST_COLUMN_SOURCE, &source, -1);
						if (source == GAMELIST_SOURCE_METASERVER && egameid == gameid && eserverid == serverid)  {
							gtk_list_store_remove(global->game_store, &iter);
							break;
						}

						valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(global->game_store), &iter);
					}
				}
			}
		}
		else if(!xmlStrcmp(cur->name, (xmlChar*)"deleteserver") )  {
			gint32 id;
			GtkTreeIter iter;
			bool valid;

			tmp = xmlGetProp(cur, (xmlChar*)"id");
			if (!tmp) {
				continue;
			}
			id = atoi((gchar*)tmp);
			g_free(tmp);

			valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(global->server_store), &iter);
			while (valid) {
				gint32 eid;

				gtk_tree_model_get(GTK_TREE_MODEL(global->server_store), &iter, SERVERLIST_COLUMN_SERVERID, &eid, -1);
				if (eid == id) {
					gtk_list_store_remove(global->server_store, &iter);
					break;
				}

				valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(global->server_store), &iter);
			}

			valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(global->game_store), &iter);
			while (valid) {
				gint32 source;
				gint32 eid;
				GtkTreeIter curiter = iter;

				valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(global->game_store), &iter); /* get next iter so we can safely remove the current entry */

				gtk_tree_model_get(GTK_TREE_MODEL(global->game_store), &curiter, GAMELIST_COLUMN_SERVERID, &eid, GAMELIST_COLUMN_SOURCE, &source, -1);
				if (source == GAMELIST_SOURCE_METASERVER && eid == id)  {
					gtk_list_store_remove(global->game_store, &curiter);
				}
			}
		}

	}

	xmlFreeDoc(doc);
}

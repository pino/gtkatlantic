/*
 *     gtkatlantic - the gtk+ monopd client, enjoy network monopoly games
 *
 *
 *  Copyright © 2002-2015 Sylvain Rochet
 *
 *  gtkatlantic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; see the file COPYING. If not, see
 *  <http://www.gnu.org/licenses/>.
 */

#ifndef TRADE_H
#define TRADE_H

#include <gtk/gtk.h>


void trade_initnew(gint32 tradeid);
void trade_destroy(gint32 tradeid);
void trade_destroy_slot(gint32 tradeslot);
void trade_create_panel(gint32 tradeslot);
void trade_rebuild_playerlist(gint32 tradeslot);
void trade_rebuild_component(gint32 tradeslot);
void trade_rebuild_subcomponent(gint32 tradeslot);
void trade_update_revision(gint32 tradeid, gint32 revision);
void trade_update_player(gint32 tradeid, gint32 playerid, gboolean accept);
void trade_update_card(gint32 tradeid, gint32 cardid, gint32 targetplayer);
void trade_update_estate(gint32 tradeid, gint32 estateid, gint32 targetplayer);
void trade_update_money(gint32 tradeid, gint32 playerfrom, gint32 playerto, gint32 money);

#endif /* TRADE_H */

/*
 *     gtkatlantic - the gtk+ monopd client, enjoy network monopoly games
 *
 *
 *  Copyright © 2002-2015 Sylvain Rochet
 *
 *  gtkatlantic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; see the file COPYING. If not, see
 *  <http://www.gnu.org/licenses/>.
 */

#ifndef XMLPARSE_H
#define XMLPARSE_H

#include <gtk/gtk.h>
#include <libxml/parser.h>

#include "global.h"

#define SERVER_XMLROOTELEMENT (xmlChar*)"monopd"
#define METASERVER_XMLROOTELEMENT (xmlChar*)"meta_atlantic"

void xmlparse_getgamelist_plugger(connection *c, gchar *buffer);
void xmlparse_game_plugger(connection *c, gchar *buffer);

void xmlparse_server(connection *c, xmlDocPtr doc, xmlNodePtr cur);
void xmlparse_client(connection *c, xmlDocPtr doc, xmlNodePtr cur);
void xmlparse_gameupdate(connection *c, xmlDocPtr doc, xmlNodePtr cur);
void xmlparse_deletegame(connection *c, xmlDocPtr doc, xmlNodePtr cur);
void xmlparse_message(connection *c, xmlDocPtr doc, xmlNodePtr cur);
void xmlparse_estateupdate(connection *c, xmlDocPtr doc, xmlNodePtr cur);
void xmlparse_playerupdate(connection *c, xmlDocPtr doc, xmlNodePtr cur);
void xmlparse_auctionupdate(connection *c, xmlDocPtr doc, xmlNodePtr cur);
void xmlparse_display(connection *c, xmlDocPtr doc, xmlNodePtr cur);
void xmlparse_configupdate(connection *c, xmlDocPtr doc, xmlNodePtr cur);
void xmlparse_tradeupdate(connection *c, xmlDocPtr doc, xmlNodePtr cur);
void xmlparse_cardupdate(connection *c, xmlDocPtr doc, xmlNodePtr cur);
void xmlparse_estategroupupdate(connection *c, xmlDocPtr doc, xmlNodePtr cur);
void xmlparse_deleteplayer(connection *c, xmlDocPtr doc, xmlNodePtr cur);

void xmlparse_gamelist_gameupdate(connection *c, xmlDocPtr doc, xmlNodePtr cur);
void xmlparse_gamelist_deletegame(connection *c, xmlDocPtr doc, xmlNodePtr cur);

void xmlparse_metaserver(connection *c, gchar *buffer);

#endif /* XMLPARSE_H */

/*
 *     gtkatlantic - the gtk+ monopd client, enjoy network monopoly games
 *
 *
 *  Copyright © 2002-2015 Sylvain Rochet
 *
 *  gtkatlantic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; see the file COPYING. If not, see
 *  <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>

#include <gtk/gtk.h>
#include <gdk/gdk.h>

#include "global.h"
#include "interface.h"
#include "load.h"
#include "client.h"
#include "game.h"

#include "engine.h"

#include "parser.h"

#ifdef WIN32
#include <winsock2.h>
#endif

/* Globals */
player *player_h;  /* Player list */
game *currentgame;  /* Currently played game entry */
game *game_h;  /* Game list */
_data *data; /* UI data  */
_global *global; /* Global state */
_config *config; /* Configuration */

int main(int argc, char **argv)  {

#ifdef WIN32
	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(1, 1), &wsaData) != 0) exit(-1);
#endif

	global = g_malloc0( sizeof(_global) );
	data = g_malloc0( sizeof(_data) );
	config = g_malloc0( sizeof(_config) );

	create_home_directory();
	read_config_files();

	gtk_init(&argc, &argv);

	eng_init();

	if( xmlparse_file_interface() ) exit(-1);

	interface_create_mainwindow();
	interface_create_getgamespage(true);

	gtk_main();

	game_exit();
#ifdef WIN32
	WSACleanup();
#endif
	return 0;
}

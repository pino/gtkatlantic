/*
 *     gtkatlantic - the gtk+ monopd client, enjoy network monopoly games
 *
 *
 *  Copyright © 2002-2015 Sylvain Rochet
 *
 *  gtkatlantic is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; see the file COPYING. If not, see
 *  <http://www.gnu.org/licenses/>.
 */

#ifndef CALLBACK_H
#define CALLBACK_H

void Callback_ShowConfiguration(GtkWidget* menuitem);
void Callback_ConnectMetaServer(GtkWidget* button);
void Callback_GetGame_Select(GtkTreeSelection *selection, gpointer data);
void Callback_ConnectButton(GtkWidget* clist);
void Callback_StartButton(GtkWidget* button);
void Callback_SendButton(GtkWidget* Button, GtkWidget *entry);
void CallBack_ok_button_configwindow(GtkWidget* widget);
void CallBack_configwindow_destroyed(GtkWidget* widget);
void CallBack_button_command_pressed(GtkWidget* button);
void Callback_AuctionButton_Absolute(GtkWidget* button, GtkWidget* entry);
void Callback_AuctionButton_Relative(GtkWidget* button);
void Callback_ClicOnEstate(GtkWidget* widget, GdkEvent *event);
void CallBack_PopupMenuEstate(GtkWidget* widget);
void Callback_EnterAnEstate(GtkWidget* widget);
void Callback_LeaveAnEstate(GtkWidget* widget);
void Callback_ClicOnPlayer(GtkWidget* widget, GdkEvent *event);
void CallBack_PopupMenuPlayer(GtkWidget* widget);
void Callback_toggle_boolean_gameoption(GtkWidget* widget);
void CallBack_menu_connect_to_server(GtkWidget* widget);
void CallBack_response_connect_to_server_window(GtkWidget* widget, gint response, GtkWidget *Win);
void CallBack_menu_public_server_list(GtkWidget* widget);
void CallBack_menu_change_nickname(GtkWidget* widget);
void CallBack_response_changenick_window(GtkWidget* widget, gint response,
		GtkWidget *Win);
void CallBack_menu_reconnect(GtkWidget* widget);
void CallBack_menu_AddPlayer(GtkWidget *widget);
void CallBack_trade_button(GtkWidget *button);
void CallBack_trade_sub_component(GtkWidget *combo);
void CallBack_trade_update_component(GtkWidget *button);
void CallBack_get_games_page_destroyed(GtkWidget *widget);
void CallBack_menu_LeaveGame(GtkWidget* widget);
void CallBack_estates_tree(GtkWidget* widget);
void CallBack_EstateTree_SelectRow(GtkTreeSelection *selection, gpointer data);
void CallBack_EstateTree_Destroyed(GtkWidget *widget);
void Callback_ProposalList_Select(GtkTreeSelection *selection, gpointer data);
void CallBack_HelpWin_Destroyed(GtkWidget *widget);
void Callback_menu_Themes(GtkWidget *widget);
void CallBack_ThemeWin_Delete(GtkWidget *widget);
void CallBack_ThemeWin_Destroyed(GtkWidget *widget);
void Callback_ThemeList_Select(GtkTreeSelection *selection, gpointer data);
void CallBack_Theme_Apply(GtkWidget* button);

#endif /* CALLBACK_H */
